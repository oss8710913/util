# Util

This library offers a numer of convenient constructs.


## Installation

Install this library by downloading it

    git clone https://gitlab.com/oss8710913/util

and executing

    ./gradlew publishToMavenLocal

in its root folder.


## Usage

Add the following to the 'build.gradle' file of your project.

	repositories {
		...
		mavenLocal()
		...
	}

	dependencies {
		...
		implementation('nl.novit.util:util:1.4.8')
		...
	}


## Support
Please use 'GitLab Issues' for support on this library. Support is not guaranteed.
