package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTuple2Comparable
{
	@Test
	public void test()
	{
		Assertions.assertEquals(-1, new Tuple2Comparable<>(0, 0).compareTo(new Tuple2Comparable<>(0, 1)));
		Assertions.assertEquals(-1, new Tuple2Comparable<>(0, 0).compareTo(new Tuple2Comparable<>(1, 0)));
		Assertions.assertEquals(-1, new Tuple2Comparable<>(0, 1).compareTo(new Tuple2Comparable<>(1, 0)));
		Assertions.assertEquals(0, new Tuple2Comparable<>(0, 0).compareTo(new Tuple2Comparable<>(0, 0)));
		Assertions.assertEquals(1, new Tuple2Comparable<>(0, 1).compareTo(new Tuple2Comparable<>(0, 0)));
		Assertions.assertEquals(1, new Tuple2Comparable<>(1, 0).compareTo(new Tuple2Comparable<>(0, 0)));
		Assertions.assertEquals(1, new Tuple2Comparable<>(1, 0).compareTo(new Tuple2Comparable<>(0, 1)));
	}
}