package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TestAlgorithm
{
	public void testBinarySearchMinimum
		(
			final int expected,
			int start,
			int end
		)
	{
		int actual = Algorithm.binarySearchMinimum
			(
				x -> expected <= x,
				start,
				end
			);
		Assertions.assertEquals(expected, actual);
	}

	public void testBinarySearchMaximum
		(
			final int expected,
			int start,
			int end
		)
	{
		int actual = Algorithm.binarySearchMaximum
			(
				x -> x <= expected,
				start,
				end
			);
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void testBinarySearchMinimum()
	{
		testBinarySearchMinimum(10, 10, 10);
		testBinarySearchMinimum(10, 10, 11);
		testBinarySearchMinimum(11, 10, 11);
		testBinarySearchMinimum(10, 10, 12);
		testBinarySearchMinimum(11, 10, 12);
		testBinarySearchMinimum(12, 10, 12);
		testBinarySearchMinimum(10, 0, 20);
	}

	@Test
	public void testBinarySearchMaximum()
	{
		testBinarySearchMaximum(9, 10, 10);
		testBinarySearchMaximum(10, 10, 11);
		testBinarySearchMaximum(9, 10, 11);
		testBinarySearchMaximum(10, 10, 12);
		testBinarySearchMaximum(11, 10, 12);
		testBinarySearchMaximum(9, 10, 12);
		testBinarySearchMaximum(10, 0, 20);
	}

	@Test
	public void testCombinationsEmpty()
	{
		List<Character> list = List.of();
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 0).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of(), combinations.next()));
		Assertions.assertFalse(combinations.hasNext());
	}

	@Test
	public void testCombinations0()
	{
		List<Character> list = List.of('a', 'b', 'c', 'd', 'e');
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 0).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of(), combinations.next()));
		Assertions.assertFalse(combinations.hasNext());
	}

	@Test
	public void testCombinations1()
	{
		List<Character> list = List.of('a', 'b', 'c', 'd', 'e');
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 1).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of('a'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('d'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('e'), combinations.next()));
		Assertions.assertFalse(combinations.hasNext());
	}

	@Test
	public void testCombinations2()
	{
		List<Character> list = List.of('a', 'b', 'c', 'd', 'e');
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 2).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'c'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'c'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('d', 'e'), combinations.next()));
		Assertions.assertFalse(combinations.hasNext());
	}

	@Test
	public void testCombinations3()
	{
		List<Character> list = List.of('a', 'b', 'c', 'd', 'e');
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 3).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'c'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'c', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'c', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'd', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'c', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'c', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'd', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'd', 'e'), combinations.next()));
		Assertions.assertFalse(combinations.hasNext());
	}

	@Test
	public void testCombinations4()
	{
		List<Character> list = List.of('a', 'b', 'c', 'd', 'e');
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 4).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'c', 'd'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'c', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'd', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'c', 'd', 'e'), combinations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'c', 'd', 'e'), combinations.next()));
	}

	@Test
	public void testCombinations5()
	{
		List<Character> list = List.of('a', 'b', 'c', 'd', 'e');
		Iterator<List<Character>> combinations = Algorithm.combinations(list, 5).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'c', 'd', 'e'), combinations.next()));
	}

	@Test
	public void testFaculty()
	{
		Assertions.assertEquals(1L, Algorithm.faculty(0));
		Assertions.assertEquals(1L, Algorithm.faculty(1));
		Assertions.assertEquals(2L, Algorithm.faculty(2));
		Assertions.assertEquals(2L * 3L, Algorithm.faculty(3));
		Assertions.assertEquals(2L * 3L * 4L, Algorithm.faculty(4));
	}

	@Test
	public void testGcd()
	{
		Assertions.assertEquals(1, Algorithm.gcd(1, 1));
		Assertions.assertEquals(1, Algorithm.gcd(1, 2));
		Assertions.assertEquals(2, Algorithm.gcd(2, 2));
		Assertions.assertEquals(1, Algorithm.gcd(2, 3));
		Assertions.assertEquals(2, Algorithm.gcd(4, 6));
	}

	@Test
	public void testPermutation()
	{
		List<Character> list = Arrays.asList('a', 'b', 'c');
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'c'), Algorithm.permutation(0, list)));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'a', 'c'), Algorithm.permutation(1, list)));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'a', 'b'), Algorithm.permutation(2, list)));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'c', 'b'), Algorithm.permutation(3, list)));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'c', 'a'), Algorithm.permutation(4, list)));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'b', 'a'), Algorithm.permutation(5, list)));
	}

	@Test
	public void testPermutations()
	{
		List<Character> list = Arrays.asList('a', 'b', 'c');
		Iterator<List<Character>> permutations = Algorithm.permutations(list).iterator();
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b', 'c'), permutations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'a', 'c'), permutations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'a', 'b'), permutations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'c', 'b'), permutations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b', 'c', 'a'), permutations.next()));
		Assertions.assertEquals(0, Util.compareIterable(List.of('c', 'b', 'a'), permutations.next()));
		Assertions.assertFalse(permutations.hasNext());
	}
}