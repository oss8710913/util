package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

public class TestCollectorSingle
{
	@Test
	public void testCorrect()
	{
		Integer actual = Stream.of(1)
			.collect(new CollectorSingle<>());
		Assertions.assertEquals(1, actual);
	}

	@Test
	public void testIncorrectEmpty()
	{
		Assertions.assertThrows
			(
				NoSuchElementException.class,
				() -> Stream.empty()
					.collect(new CollectorSingle<>())
			);
	}

	@Test
	public void testIncorrectAmbiguous()
	{
		Assertions.assertThrows
			(
				NoSuchElementException.class,
				() -> Stream.of(1, 2)
					.collect(new CollectorSingle<>())
			);
	}
}