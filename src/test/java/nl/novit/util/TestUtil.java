package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TestUtil
{
	@Test
	public void testGetCanonicalName()
	{
		TestUtil testUtil = new TestUtil();
		TestUtil anonymous = new TestUtil()
		{
		};
		Assertions.assertEquals(testUtil.getClass().getCanonicalName(), Util.getCanonicalName(TestUtil.class));
		Assertions.assertNull(anonymous.getClass().getCanonicalName());
		Assertions.assertEquals(testUtil.getClass().getCanonicalName(), Util.getCanonicalName(anonymous.getClass()));
	}

	@Test
	public void testEqualsNullable()
	{
		Assertions.assertTrue(Util.equalsNullable(null, null));
		Assertions.assertFalse(Util.equalsNullable(null, 1));
		Assertions.assertFalse(Util.equalsNullable(1, null));
		Assertions.assertTrue(Util.equalsNullable(0, 0));
		Assertions.assertFalse(Util.equalsNullable(0, 1));
	}

	@Test
	public void testCompareNullable()
	{
		Assertions.assertEquals(0, Util.compareNullable(null, null));
		Assertions.assertEquals(-1, Util.compareNullable(null, 1));
		Assertions.assertEquals(1, Util.compareNullable(1, null));
		Assertions.assertEquals(0, Util.compareNullable(0, 0));
		Assertions.assertEquals(-1, Util.compareNullable(0, 1));
		Assertions.assertEquals(1, Util.compareNullable(1, 0));
	}

	@Test
	public void testCompareNullableComparator()
	{
		final Object a = new Object();
		final Object b = new Object();
		Comparator<Object> comparator = new Comparator<>()
		{
			@Override
			public int compare(Object object0, Object object1)
			{
				int result;
				if (object0 == a)
				{
					if (object1 == a)
					{
						result = 0;
					}
					else
					{
						result = -1;
					}
				}
				else
				{
					if (object1 == a)
					{
						result = 1;
					}
					else
					{
						result = 0;
					}
				}
				return result;
			}
		};
		Assertions.assertEquals(0, Util.compareNullable(comparator, null, null));
		Assertions.assertEquals(-1, Util.compareNullable(comparator, null, b));
		Assertions.assertEquals(1, Util.compareNullable(comparator, b, null));
		Assertions.assertEquals(0, Util.compareNullable(comparator, b, b));
		Assertions.assertEquals(-1, Util.compareNullable(comparator, a, b));
		Assertions.assertEquals(1, Util.compareNullable(comparator, b, a));
	}

	@Test
	public void testCompareIterable()
	{
		Assertions.assertEquals(0, Util.compareIterable(new ArrayList<>(), new ArrayList<Integer>()));
		Assertions.assertEquals(-1, Util.compareIterable(new ArrayList<>(), List.of(0)));
		Assertions.assertEquals(1, Util.compareIterable(List.of(0), new ArrayList<>()));
		Assertions.assertEquals(0, Util.compareIterable(List.of(0), List.of(0)));
		Assertions.assertEquals(-1, Util.compareIterable(List.of(0), List.of(1)));
		Assertions.assertEquals(1, Util.compareIterable(List.of(1), List.of(0)));
		Assertions.assertEquals(0, Util.compareIterable(List.of(0, 1), List.of(0, 1)));
		Assertions.assertEquals(-1, Util.compareIterable(List.of(0, 0), List.of(0, 1)));
		Assertions.assertEquals(1, Util.compareIterable(List.of(0, 1), List.of(0, 0)));
		Assertions.assertEquals(-1, Util.compareIterable(List.of(0, 1), List.of(1, 0)));
		Assertions.assertEquals(1, Util.compareIterable(List.of(1, 0), List.of(0, 1)));
	}

	@Test
	public void testCompareIterableComparator()
	{
		class X
		{
			final int x;

			public X(int x)
			{
				this.x = x;
			}

			public int getX()
			{
				return x;
			}
		}
		Assertions.assertEquals(-1, Util.compareIterable(Comparator.comparing(X::getX), List.of(new X(0)), List.of(new X(1))));
		Assertions.assertEquals(0, Util.compareIterable(Comparator.comparing(X::getX), List.of(new X(0)), List.of(new X(0))));
		Assertions.assertEquals(1, Util.compareIterable(Comparator.comparing(X::getX), List.of(new X(1)), List.of(new X(0))));
	}

	@Test
	public void testEqualsIterator()
	{
		Assertions.assertTrue(Util.equalsIterable(List.of(), List.of()));
		Assertions.assertTrue(Util.equalsIterable(List.of(1), List.of(1)));
		Assertions.assertTrue(Util.equalsIterable(List.of(1, 2), List.of(1, 2)));
		Assertions.assertFalse(Util.equalsIterable(List.of(), List.of(1)));
		Assertions.assertFalse(Util.equalsIterable(List.of(1), List.of(1, 2)));
		Assertions.assertFalse(Util.equalsIterable(List.of(new Object()), List.of(new Object())));
	}

	@Test
	public void testConvert()
	{
		Assertions.assertEquals(List.of(), Util.convert(""));
		Assertions.assertEquals("", Util.convert(List.of()));
		Assertions.assertEquals(List.of('a'), Util.convert("a"));
		Assertions.assertEquals("a", Util.convert(List.of('a')));
		Assertions.assertEquals(List.of('a', 'b'), Util.convert("ab"));
		Assertions.assertEquals("ab", Util.convert(List.of('a', 'b')));
	}

	@Test
	public void testAnd()
	{
		boolean test = false;
		Assertions.assertFalse(false);
		Assertions.assertFalse(test);
		Assertions.assertTrue(Util.and(true, (test = true)));
		Assertions.assertTrue(test);
	}

	@Test
	public void testOr()
	{
		boolean test = false;
		Assertions.assertTrue(true);
		Assertions.assertFalse(test);
		Assertions.assertTrue(Util.or(true, (test = true)));
		Assertions.assertTrue(test);
	}

	@Test
	public void testToString()
	{
		Assertions.assertEquals("", Util.toString(Collections.emptyList(), ""));
		Assertions.assertEquals("1", Util.toString(List.of(1), ""));
		Assertions.assertEquals("12", Util.toString(List.of(1, 2), ""));
		Assertions.assertEquals("", Util.toString(Collections.emptyList(), "-"));
		Assertions.assertEquals("1", Util.toString(List.of(1), "-"));
		Assertions.assertEquals("1, 2", Util.toString(List.of(1, 2), ", "));
		Assertions.assertEquals("1-2", Util.toString(List.of(1, 2), '-'));
		Assertions.assertEquals("1", Util.toString(List.of(1), "", x -> String.valueOf(x * x)));
		Assertions.assertEquals("1, 4", Util.toString(List.of(1, 2), ", ", x -> String.valueOf(x * x)));
		Assertions.assertEquals("(1, 2)", Util.toString(List.of(1, 2), ", ", '(', ')'));
		Assertions.assertEquals("", Util.toString(Collections.emptyList()));
		Assertions.assertEquals("1", Util.toString(List.of(1)));
		Assertions.assertEquals("12", Util.toString(List.of(1, 2)));
	}
}