package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestUtilCollections
{
	@Test
	public void testListOf1()
	{
		Character value0 = 'a';
		List<Character> list = UtilCollections.listOf(value0);
		Assertions.assertEquals(1, list.size());
		Assertions.assertEquals(value0, list.get(0));
	}

	@Test
	public void testListOf2()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		List<Character> list = UtilCollections.listOf(value0, value1);
		Assertions.assertEquals(2, list.size());
		Assertions.assertEquals(value0, list.get(0));
		Assertions.assertEquals(value1, list.get(1));
	}

	@Test
	public void testListOf3()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Character value2 = 'c';
		List<Character> list = UtilCollections.listOf(value0, value1, value2);
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals(value0, list.get(0));
		Assertions.assertEquals(value1, list.get(1));
		Assertions.assertEquals(value2, list.get(2));
	}

	@Test
	public void testListOf4()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Character value2 = 'c';
		Character value3 = 'd';
		List<Character> list = UtilCollections.listOf(value0, value1, value2, value3);
		Assertions.assertEquals(4, list.size());
		Assertions.assertEquals(value0, list.get(0));
		Assertions.assertEquals(value1, list.get(1));
		Assertions.assertEquals(value2, list.get(2));
		Assertions.assertEquals(value3, list.get(3));
	}

	@Test
	public void testListOf5()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Character value2 = 'c';
		Character value3 = 'd';
		Character value4 = 'e';
		List<Character> list = UtilCollections.listOf(value0, value1, value2, value3, value4);
		Assertions.assertEquals(5, list.size());
		Assertions.assertEquals(value0, list.get(0));
		Assertions.assertEquals(value1, list.get(1));
		Assertions.assertEquals(value2, list.get(2));
		Assertions.assertEquals(value3, list.get(3));
		Assertions.assertEquals(value4, list.get(4));
	}

	@Test
	public void testSetOf1()
	{
		Character value0 = 'a';
		Set<Character> set = UtilCollections.setOf(value0);
		Assertions.assertEquals(1, set.size());
		Assertions.assertTrue(set.contains(value0));
	}

	@Test
	public void testSetOf2()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Set<Character> set = UtilCollections.setOf(value0, value1);
		Assertions.assertEquals(2, set.size());
		Assertions.assertTrue(set.contains(value0));
		Assertions.assertTrue(set.contains(value1));
	}

	@Test
	public void testSetOf3()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Character value2 = 'c';
		Set<Character> set = UtilCollections.setOf(value0, value1, value2);
		Assertions.assertEquals(3, set.size());
		Assertions.assertTrue(set.contains(value0));
		Assertions.assertTrue(set.contains(value1));
		Assertions.assertTrue(set.contains(value2));
	}

	@Test
	public void testSetOf4()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Character value2 = 'c';
		Character value3 = 'd';
		Set<Character> set = UtilCollections.setOf(value0, value1, value2, value3);
		Assertions.assertEquals(4, set.size());
		Assertions.assertTrue(set.contains(value0));
		Assertions.assertTrue(set.contains(value1));
		Assertions.assertTrue(set.contains(value2));
		Assertions.assertTrue(set.contains(value3));
	}

	@Test
	public void testSetOf5()
	{
		Character value0 = 'a';
		Character value1 = 'b';
		Character value2 = 'c';
		Character value3 = 'd';
		Character value4 = 'e';
		Set<Character> set = UtilCollections.setOf(value0, value1, value2, value3, value4);
		Assertions.assertEquals(5, set.size());
		Assertions.assertTrue(set.contains(value0));
		Assertions.assertTrue(set.contains(value1));
		Assertions.assertTrue(set.contains(value2));
		Assertions.assertTrue(set.contains(value3));
		Assertions.assertTrue(set.contains(value4));
	}

	@Test
	public void testMapOf1()
	{
		Integer key0 = 1;
		Character value0 = 'a';
		Map<Integer, Character> map = UtilCollections.mapOf(key0, value0);
		Assertions.assertEquals(1, map.size());
		Assertions.assertEquals(value0, map.get(key0));
	}

	@Test
	public void testMapOf2()
	{
		Integer key0 = 1;
		Character value0 = 'a';
		Integer key1 = 2;
		Character value1 = 'b';
		Map<Integer, Character> map = UtilCollections.mapOf(key0, value0, key1, value1);
		Assertions.assertEquals(2, map.size());
		Assertions.assertEquals(value0, map.get(key0));
		Assertions.assertEquals(value1, map.get(key1));
	}

	@Test
	public void testMapOf3()
	{
		Integer key0 = 1;
		Character value0 = 'a';
		Integer key1 = 2;
		Character value1 = 'b';
		Integer key2 = 3;
		Character value2 = 'c';
		Map<Integer, Character> map = UtilCollections.mapOf(key0, value0, key1, value1, key2, value2);
		Assertions.assertEquals(3, map.size());
		Assertions.assertEquals(value0, map.get(key0));
		Assertions.assertEquals(value1, map.get(key1));
		Assertions.assertEquals(value2, map.get(key2));
	}

	@Test
	public void testMapOf4()
	{
		Integer key0 = 1;
		Character value0 = 'a';
		Integer key1 = 2;
		Character value1 = 'b';
		Integer key2 = 3;
		Character value2 = 'c';
		Integer key3 = 4;
		Character value3 = 'd';
		Map<Integer, Character> map = UtilCollections.mapOf(key0, value0, key1, value1, key2, value2, key3, value3);
		Assertions.assertEquals(4, map.size());
		Assertions.assertEquals(value0, map.get(key0));
		Assertions.assertEquals(value1, map.get(key1));
		Assertions.assertEquals(value2, map.get(key2));
		Assertions.assertEquals(value3, map.get(key3));
	}

	@Test
	public void testMapOf5()
	{
		Integer key0 = 0;
		Character value0 = 'a';
		Integer key1 = 1;
		Character value1 = 'b';
		Integer key2 = 2;
		Character value2 = 'c';
		Integer key3 = 3;
		Character value3 = 'd';
		Integer key4 = 4;
		Character value4 = 'e';
		Map<Integer, Character> map = UtilCollections.mapOf(key0, value0, key1, value1, key2, value2, key3, value3, key4, value4);
		Assertions.assertEquals(5, map.size());
		Assertions.assertEquals(value0, map.get(key0));
		Assertions.assertEquals(value1, map.get(key1));
		Assertions.assertEquals(value2, map.get(key2));
		Assertions.assertEquals(value3, map.get(key3));
		Assertions.assertEquals(value4, map.get(key4));
	}

	@Test
	public void testUnion2()
	{
		Assertions.assertEquals(UtilCollections.setOf('a', 'b'), UtilCollections.union(UtilCollections.setOf('a'), UtilCollections.setOf('b')));
	}

	@Test
	public void testUnion3()
	{
		Assertions.assertEquals(UtilCollections.setOf('a', 'b', 'c'), UtilCollections.union(UtilCollections.setOf('a'), UtilCollections.setOf('b'), UtilCollections.setOf('c')));
	}

	@Test
	public void testUnion4()
	{
		Assertions.assertEquals(UtilCollections.setOf('a', 'b', 'c', 'd'), UtilCollections.union(UtilCollections.setOf('a'), UtilCollections.setOf('b'), UtilCollections.setOf('c'), UtilCollections.setOf('d')));
	}

	@Test
	public void testUnion5()
	{
		Assertions.assertEquals(UtilCollections.setOf('a', 'b', 'c', 'd', 'e'), UtilCollections.union(UtilCollections.setOf('a'), UtilCollections.setOf('b'), UtilCollections.setOf('c'), UtilCollections.setOf('d'), UtilCollections.setOf('e')));
	}
}