package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

public class TestFraction
{
	public static final Fraction FRACTION_MINUS_ONE = new Fraction(BigInteger.valueOf(-1));
	public static final Fraction FRACTION_ZERO = new Fraction(BigInteger.ZERO);
	public static final Fraction FRACTION_HALF = new Fraction(BigInteger.ONE, BigInteger.valueOf(2));
	public static final Fraction FRACTION_TWO_THIRD = new Fraction(BigInteger.valueOf(2), BigInteger.valueOf(3));
	public static final Fraction FRACTION_ONE = new Fraction(BigInteger.ONE);
	public static final Fraction FRACTION_TWO = new Fraction(BigInteger.valueOf(2));

	@Test
	public void testGcd()
	{
		Assertions.assertEquals(BigInteger.ZERO, Fraction.gcd(BigInteger.ZERO, BigInteger.ZERO));
		Assertions.assertEquals(BigInteger.ONE, Fraction.gcd(BigInteger.ZERO, BigInteger.ONE));
		Assertions.assertEquals(BigInteger.ONE, Fraction.gcd(BigInteger.ONE, BigInteger.ZERO));
		Assertions.assertEquals(BigInteger.ONE, Fraction.gcd(BigInteger.ONE, BigInteger.ONE));
		Assertions.assertEquals(BigInteger.ONE, Fraction.gcd(BigInteger.ONE, BigInteger.valueOf(2)));
		Assertions.assertEquals(BigInteger.ONE, Fraction.gcd(BigInteger.valueOf(2), BigInteger.ONE));
		Assertions.assertEquals(BigInteger.ONE, Fraction.gcd(BigInteger.valueOf(2), BigInteger.valueOf(3)));
		Assertions.assertEquals(BigInteger.valueOf(2), Fraction.gcd(BigInteger.valueOf(2), BigInteger.valueOf(4)));
		Assertions.assertEquals(BigInteger.valueOf(2), Fraction.gcd(BigInteger.valueOf(4), BigInteger.valueOf(6)));
	}

	@Test
	public void testFraction()
	{
		Assertions.assertEquals(BigInteger.valueOf(-1), FRACTION_MINUS_ONE.nominator);
		Assertions.assertEquals(BigInteger.ONE, FRACTION_MINUS_ONE.denominator);
		Assertions.assertEquals(BigInteger.valueOf(-1), new Fraction(BigInteger.ONE, BigInteger.valueOf(-1)).nominator);
		Assertions.assertEquals(BigInteger.ONE, new Fraction(BigInteger.ONE, BigInteger.valueOf(-1)).denominator);
		Assertions.assertEquals(BigInteger.ZERO, FRACTION_ZERO.nominator);
		Assertions.assertEquals(BigInteger.ONE, FRACTION_ZERO.denominator);
		Assertions.assertEquals(BigInteger.ONE, FRACTION_ONE.nominator);
		Assertions.assertEquals(BigInteger.ONE, FRACTION_ONE.denominator);
		Assertions.assertEquals(BigInteger.ONE, new Fraction(BigInteger.ONE, BigInteger.ONE).nominator);
		Assertions.assertEquals(BigInteger.ONE, new Fraction(BigInteger.ONE, BigInteger.ONE).denominator);
		Assertions.assertEquals(BigInteger.valueOf(2), new Fraction(BigInteger.valueOf(2), BigInteger.ONE).nominator);
		Assertions.assertEquals(BigInteger.ONE, new Fraction(BigInteger.valueOf(2), BigInteger.ONE).denominator);
		Assertions.assertEquals(BigInteger.ONE, FRACTION_HALF.nominator);
		Assertions.assertEquals(BigInteger.valueOf(2), FRACTION_HALF.denominator);
		Assertions.assertEquals(BigInteger.ONE, new Fraction(BigInteger.valueOf(2), BigInteger.valueOf(4)).nominator);
		Assertions.assertEquals(BigInteger.valueOf(2), new Fraction(BigInteger.valueOf(2), BigInteger.valueOf(4)).denominator);
	}

	@Test
	public void testCompareTo()
	{
		Assertions.assertEquals(0, FRACTION_MINUS_ONE.compareTo(FRACTION_MINUS_ONE));
		Assertions.assertEquals(-1, FRACTION_MINUS_ONE.compareTo(FRACTION_ZERO));
		Assertions.assertEquals(-1, FRACTION_MINUS_ONE.compareTo(FRACTION_HALF));
		Assertions.assertEquals(-1, FRACTION_MINUS_ONE.compareTo(FRACTION_TWO_THIRD));
		Assertions.assertEquals(-1, FRACTION_MINUS_ONE.compareTo(FRACTION_ONE));
		Assertions.assertEquals(-1, FRACTION_MINUS_ONE.compareTo(FRACTION_TWO));
		Assertions.assertEquals(1, FRACTION_ZERO.compareTo(FRACTION_MINUS_ONE));
		Assertions.assertEquals(0, FRACTION_ZERO.compareTo(FRACTION_ZERO));
		Assertions.assertEquals(-1, FRACTION_ZERO.compareTo(FRACTION_HALF));
		Assertions.assertEquals(-1, FRACTION_ZERO.compareTo(FRACTION_TWO_THIRD));
		Assertions.assertEquals(-1, FRACTION_ZERO.compareTo(FRACTION_ONE));
		Assertions.assertEquals(-1, FRACTION_ZERO.compareTo(FRACTION_TWO));
		Assertions.assertEquals(1, FRACTION_HALF.compareTo(FRACTION_MINUS_ONE));
		Assertions.assertEquals(1, FRACTION_HALF.compareTo(FRACTION_ZERO));
		Assertions.assertEquals(0, FRACTION_HALF.compareTo(FRACTION_HALF));
		Assertions.assertEquals(-1, FRACTION_HALF.compareTo(FRACTION_TWO_THIRD));
		Assertions.assertEquals(-1, FRACTION_HALF.compareTo(FRACTION_ONE));
		Assertions.assertEquals(-1, FRACTION_HALF.compareTo(FRACTION_TWO));
		Assertions.assertEquals(1, FRACTION_TWO_THIRD.compareTo(FRACTION_MINUS_ONE));
		Assertions.assertEquals(1, FRACTION_TWO_THIRD.compareTo(FRACTION_ZERO));
		Assertions.assertEquals(1, FRACTION_TWO_THIRD.compareTo(FRACTION_HALF));
		Assertions.assertEquals(0, FRACTION_TWO_THIRD.compareTo(FRACTION_TWO_THIRD));
		Assertions.assertEquals(-1, FRACTION_TWO_THIRD.compareTo(FRACTION_ONE));
		Assertions.assertEquals(-1, FRACTION_TWO_THIRD.compareTo(FRACTION_TWO));
		Assertions.assertEquals(1, FRACTION_ONE.compareTo(FRACTION_MINUS_ONE));
		Assertions.assertEquals(1, FRACTION_ONE.compareTo(FRACTION_ZERO));
		Assertions.assertEquals(1, FRACTION_ONE.compareTo(FRACTION_HALF));
		Assertions.assertEquals(1, FRACTION_ONE.compareTo(FRACTION_TWO_THIRD));
		Assertions.assertEquals(0, FRACTION_ONE.compareTo(FRACTION_ONE));
		Assertions.assertEquals(-1, FRACTION_ONE.compareTo(FRACTION_TWO));
		Assertions.assertEquals(1, FRACTION_TWO.compareTo(FRACTION_MINUS_ONE));
		Assertions.assertEquals(1, FRACTION_TWO.compareTo(FRACTION_ZERO));
		Assertions.assertEquals(1, FRACTION_TWO.compareTo(FRACTION_HALF));
		Assertions.assertEquals(1, FRACTION_TWO.compareTo(FRACTION_TWO_THIRD));
		Assertions.assertEquals(1, FRACTION_TWO.compareTo(FRACTION_ONE));
		Assertions.assertEquals(0, FRACTION_TWO.compareTo(FRACTION_TWO));
	}

	@Test
	public void testAdd()
	{
		Assertions.assertEquals(FRACTION_TWO, FRACTION_ONE.add(FRACTION_ONE));
		Assertions.assertEquals(FRACTION_ONE, FRACTION_HALF.add(FRACTION_HALF));
		Assertions.assertEquals(FRACTION_ONE, FRACTION_HALF.add(FRACTION_HALF));
		Assertions.assertEquals(new Fraction(BigInteger.valueOf(7), BigInteger.valueOf(6)), FRACTION_HALF.add(FRACTION_TWO_THIRD));
		Assertions.assertEquals(new Fraction(BigInteger.valueOf(4), BigInteger.valueOf(3)), FRACTION_TWO_THIRD.add(FRACTION_TWO_THIRD));
		Assertions.assertEquals(new Fraction(BigInteger.valueOf(4), BigInteger.valueOf(3)), FRACTION_TWO_THIRD.add(FRACTION_TWO_THIRD));
	}

	@Test
	public void testNegate()
	{
		Assertions.assertEquals(new Fraction(BigInteger.valueOf(-1)), FRACTION_ONE.negate());
		Assertions.assertEquals(new Fraction(BigInteger.valueOf(-1), BigInteger.valueOf(2)), FRACTION_HALF.negate());
	}

	@Test
	public void testSubtract()
	{
		Assertions.assertEquals(FRACTION_ZERO, FRACTION_ONE.subtract(FRACTION_ONE));
		Assertions.assertEquals(new Fraction(BigInteger.ONE, BigInteger.valueOf(6)), FRACTION_TWO_THIRD.subtract(FRACTION_HALF));
		Assertions.assertEquals(new Fraction(BigInteger.valueOf(-1), BigInteger.valueOf(6)), FRACTION_HALF.subtract(FRACTION_TWO_THIRD));
	}

	@Test
	public void testMultiply()
	{
		Assertions.assertEquals(FRACTION_ZERO, FRACTION_ZERO.multiply(FRACTION_ZERO));
		Assertions.assertEquals(FRACTION_ZERO, FRACTION_ZERO.multiply(FRACTION_ONE));
		Assertions.assertEquals(FRACTION_ONE, FRACTION_ONE.multiply(FRACTION_ONE));
		Assertions.assertEquals(new Fraction(BigInteger.ONE, BigInteger.valueOf(4)), FRACTION_HALF.multiply(FRACTION_HALF));
		Assertions.assertEquals(new Fraction(BigInteger.ONE, BigInteger.valueOf(3)), FRACTION_TWO_THIRD.multiply(FRACTION_HALF));
	}

	@Test
	public void testDivide()
	{
		Assertions.assertEquals(FRACTION_ZERO, FRACTION_ZERO.divide(FRACTION_ONE));
		Assertions.assertEquals(FRACTION_HALF, FRACTION_ONE.divide(FRACTION_TWO));
		Assertions.assertEquals(FRACTION_TWO, FRACTION_ONE.divide(FRACTION_HALF));
	}
}