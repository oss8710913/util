package nl.novit.util.collections.linear.stack;

import nl.novit.util.collections.set.set23.Set23Comparable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;
import java.util.Iterator;

public class TestStack
{
	public static final Stack<Character> STACK_EMPTY = new StackEmpty<>();
	public static final Stack<Character> STACK_A =
		new StackPopulated<>
			(
				'A',
				STACK_EMPTY
			);
	public static final Stack<Character> STACK_B =
		new StackPopulated<>
			(
				'B',
				STACK_EMPTY
			);
	public static final Stack<Character> STACK_AB =
		new StackPopulated<>
			(
				'B',
				STACK_A
			);

	@Test
	public void testEquals()
	{
		Assertions.assertTrue(STACK_EMPTY.equals(STACK_EMPTY));
		Assertions.assertFalse(STACK_EMPTY.equals(STACK_A));
		Assertions.assertFalse(STACK_EMPTY.equals(STACK_B));
		Assertions.assertFalse(STACK_EMPTY.equals(STACK_AB));
		Assertions.assertFalse(STACK_A.equals(STACK_EMPTY));
		Assertions.assertTrue(STACK_A.equals(STACK_A));
		Assertions.assertFalse(STACK_A.equals(STACK_B));
		Assertions.assertFalse(STACK_A.equals(STACK_AB));
		Assertions.assertFalse(STACK_B.equals(STACK_EMPTY));
		Assertions.assertFalse(STACK_B.equals(STACK_A));
		Assertions.assertTrue(STACK_B.equals(STACK_B));
		Assertions.assertFalse(STACK_B.equals(STACK_AB));
		Assertions.assertFalse(STACK_AB.equals(STACK_EMPTY));
		Assertions.assertFalse(STACK_AB.equals(STACK_A));
		Assertions.assertFalse(STACK_AB.equals(STACK_B));
		Assertions.assertTrue(STACK_AB.equals(STACK_AB));
	}

	@Test
	public void testIsEmptyFalse()
	{
		Assertions.assertFalse(STACK_A.isEmpty());
	}

	@Test
	public void testIsEmptyTrue()
	{
		Assertions.assertTrue(STACK_EMPTY.isEmpty());
	}

	@Test
	public void testIteratorEmpty()
	{
		Iterator<Character> iterator = STACK_EMPTY.iterator();
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorA()
	{
		Iterator<Character> iterator = STACK_A.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorAB()
	{
		Iterator<Character> iterator = STACK_AB.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testPeekEmpty()
	{
		Assertions.assertThrows
			(
				EmptyStackException.class,
				STACK_EMPTY::peek
			);
	}

	@Test
	public void testPeekPopulated1()
	{
		Assertions.assertEquals('A', STACK_A.peek());
	}

	@Test
	public void testPeekPopulated2()
	{
		Assertions.assertEquals('B', STACK_AB.peek());
	}

	@Test
	public void testPopEmpty()
	{
		Assertions.assertThrows
			(
				EmptyStackException.class,
				STACK_EMPTY::pop
			);
	}

	@Test
	public void testPopPopulated1()
	{
		Assertions.assertEquals(STACK_EMPTY, STACK_A.pop());
	}

	@Test
	public void testPopPopulated2()
	{
		Assertions.assertEquals(STACK_A, STACK_AB.pop());
	}

	@Test
	public void testPush1()
	{
		Assertions.assertEquals(STACK_A, STACK_EMPTY.push('A'));
	}

	@Test
	public void testPush2()
	{
		Assertions.assertEquals(STACK_AB, STACK_A.push('B'));
	}

	@Test
	public void testPushAll1()
	{
		Assertions.assertEquals(STACK_EMPTY, STACK_EMPTY.push(new Set23Comparable<>()));
	}

	@Test
	public void testPushAll2()
	{
		Assertions.assertEquals(STACK_A, STACK_EMPTY.push(new Set23Comparable<Character>().add('A')));
	}

	@Test
	public void testPushAll3()
	{
		Assertions.assertEquals(STACK_AB, STACK_EMPTY.push(new Set23Comparable<Character>().add('A').add('B')));
	}

	@Test
	public void testPushAll4()
	{
		Assertions.assertEquals(STACK_AB, STACK_A.push(new Set23Comparable<Character>().add('B')));
	}
}