package nl.novit.util.collections.linear.queue;

import nl.novit.util.collections.linear.stack.StackEmpty;
import nl.novit.util.collections.set.set23.Set23Comparable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;
import java.util.Iterator;

public class TestQueue
{
	public static final Queue<Character> QUEUE_EMPTY = new Queue<>();
	public static final Queue<Character> QUEUE_A_ = new Queue<>
		(
			new StackEmpty<Character>()
				.push('A'),
			new StackEmpty<>()
		);
	public static final Queue<Character> QUEUE_B_ = new Queue<>
		(
			new StackEmpty<Character>()
				.push('B'),
			new StackEmpty<>()
		);
	public static final Queue<Character> QUEUE_AB_ = new Queue<>
		(
			new StackEmpty<Character>()
				.push('B')
				.push('A'),
			new StackEmpty<>()
		);
	public static final Queue<Character> QUEUE_A_B = new Queue<>
		(
			new StackEmpty<Character>()
				.push('A'),
			new StackEmpty<Character>()
				.push('B')
		);

	@Test
	public void testEqualsEmpty()
	{
		Assertions.assertTrue(QUEUE_EMPTY.equals(QUEUE_EMPTY));
		Assertions.assertFalse(QUEUE_EMPTY.equals(QUEUE_A_));
		Assertions.assertFalse(QUEUE_EMPTY.equals(QUEUE_B_));
		Assertions.assertFalse(QUEUE_EMPTY.equals(QUEUE_AB_));
		Assertions.assertFalse(QUEUE_EMPTY.equals(QUEUE_A_B));
	}

	@Test
	public void testEqualsA_()
	{
		Assertions.assertFalse(QUEUE_A_.equals(QUEUE_EMPTY));
		Assertions.assertTrue(QUEUE_A_.equals(QUEUE_A_));
		Assertions.assertFalse(QUEUE_A_.equals(QUEUE_B_));
		Assertions.assertFalse(QUEUE_A_.equals(QUEUE_AB_));
		Assertions.assertFalse(QUEUE_A_.equals(QUEUE_A_B));
	}

	@Test
	public void testEqualsB_()
	{
		Assertions.assertFalse(QUEUE_B_.equals(QUEUE_EMPTY));
		Assertions.assertFalse(QUEUE_B_.equals(QUEUE_A_));
		Assertions.assertTrue(QUEUE_B_.equals(QUEUE_B_));
		Assertions.assertFalse(QUEUE_B_.equals(QUEUE_AB_));
		Assertions.assertFalse(QUEUE_B_.equals(QUEUE_A_B));
	}

	@Test
	public void testEqualsAB_()
	{
		Assertions.assertFalse(QUEUE_AB_.equals(QUEUE_EMPTY));
		Assertions.assertFalse(QUEUE_AB_.equals(QUEUE_A_));
		Assertions.assertFalse(QUEUE_AB_.equals(QUEUE_B_));
		Assertions.assertTrue(QUEUE_AB_.equals(QUEUE_AB_));
		Assertions.assertTrue(QUEUE_AB_.equals(QUEUE_A_B));
	}

	@Test
	public void testEqualsA_B()
	{
		Assertions.assertFalse(QUEUE_A_B.equals(QUEUE_EMPTY));
		Assertions.assertFalse(QUEUE_A_B.equals(QUEUE_A_));
		Assertions.assertFalse(QUEUE_A_B.equals(QUEUE_B_));
		Assertions.assertTrue(QUEUE_A_B.equals(QUEUE_AB_));
		Assertions.assertTrue(QUEUE_A_B.equals(QUEUE_A_B));
	}

	@Test
	public void testIsEmptyFalse()
	{
		Assertions.assertFalse(QUEUE_A_.isEmpty());
		Assertions.assertFalse(QUEUE_B_.isEmpty());
		Assertions.assertFalse(QUEUE_AB_.isEmpty());
		Assertions.assertFalse(QUEUE_A_B.isEmpty());
	}

	@Test
	public void testIsEmptyTrue()
	{
		Assertions.assertTrue(QUEUE_EMPTY.isEmpty());
	}

	@Test
	public void testIteratorEmpty()
	{
		Iterator<Character> iterator = QUEUE_EMPTY.iterator();
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorA_()
	{
		Iterator<Character> iterator = QUEUE_A_.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorB_()
	{
		Iterator<Character> iterator = QUEUE_B_.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorAB_()
	{
		Iterator<Character> iterator = QUEUE_AB_.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorA_B()
	{
		Iterator<Character> iterator = QUEUE_A_B.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testPeekEmpty()
	{
		Assertions.assertThrows
			(
				EmptyStackException.class,
				QUEUE_EMPTY::peek
			);
	}

	@Test
	public void testPeekA_()
	{
		Assertions.assertEquals('A', QUEUE_A_.peek());
	}

	@Test
	public void testPeekB_()
	{
		Assertions.assertEquals('B', QUEUE_B_.peek());
	}

	@Test
	public void testPeekAB_()
	{
		Assertions.assertEquals('A', QUEUE_AB_.peek());
	}

	@Test
	public void testPeekA_B_()
	{
		Assertions.assertEquals('A', QUEUE_A_B.peek());
	}

	@Test
	public void testPopEmpty()
	{
		Assertions.assertThrows
			(
				EmptyStackException.class,
				QUEUE_EMPTY::pop
			);
	}

	@Test
	public void testPopA_()
	{
		Assertions.assertEquals(QUEUE_EMPTY, QUEUE_A_.pop());
	}

	@Test
	public void testPopB_()
	{
		Assertions.assertEquals(QUEUE_EMPTY, QUEUE_B_.pop());
	}

	@Test
	public void testPopA_B()
	{
		Assertions.assertEquals(QUEUE_B_, QUEUE_AB_.pop());
	}

	@Test
	public void testPopAB_()
	{
		Assertions.assertEquals(QUEUE_B_, QUEUE_A_B.pop());
	}

	@Test
	public void testPushA_()
	{
		Assertions.assertEquals(QUEUE_A_, QUEUE_EMPTY.push('A'));
	}

	@Test
	public void testPushB_()
	{
		Assertions.assertEquals(QUEUE_B_, QUEUE_EMPTY.push('B'));
	}

	@Test
	public void testPushA_B()
	{
		Assertions.assertEquals(QUEUE_A_B, QUEUE_A_.push('B'));
	}

	@Test
	public void testPushAllEmpty()
	{
		Assertions.assertEquals(QUEUE_EMPTY, QUEUE_EMPTY.push(new Set23Comparable<>()));
	}

	@Test
	public void testPushAllA()
	{
		Assertions.assertEquals(QUEUE_A_, QUEUE_EMPTY.push(new Set23Comparable<Character>().add('A')));
	}

	@Test
	public void testPushAllB()
	{
		Assertions.assertEquals(QUEUE_A_B, QUEUE_A_.push(new Set23Comparable<Character>().add('B')));
	}

	@Test
	public void testPushAllAB()
	{
		Assertions.assertEquals(QUEUE_A_B, QUEUE_EMPTY.push(new Set23Comparable<Character>().add('A').add('B')));
	}

	@Test
	public void testPushPop()
	{
		Queue<Character> queue = new Queue<>();
		Assertions.assertTrue(queue.isEmpty());
		queue = queue.push('A');
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('A', queue.peek());
		queue = queue.push('B');
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('A', queue.peek());
		queue = queue.pop();
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('B', queue.peek());
		queue = queue.push('C');
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('B', queue.peek());
		queue = queue.push('D');
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('B', queue.peek());
		queue = queue.pop();
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('C', queue.peek());
		queue = queue.pop();
		Assertions.assertFalse(queue.isEmpty());
		Assertions.assertEquals('D', queue.peek());
		queue = queue.pop();
		Assertions.assertTrue(queue.isEmpty());
	}
}