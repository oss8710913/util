package nl.novit.util.collections.set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public abstract class TestMap
{
	public abstract Map<Character, Integer> mapEmpty();

	public abstract Map<Character, Integer> mapA2();

	public abstract Map<Character, Integer> mapA2B3();

	public abstract Map<Character, Integer> mapA2B3C1();

	public abstract Map<Character, Integer> mapA4B3C1();

	@Test
	public void testAddA2()
	{
		Assertions.assertEquals(mapA2(), mapEmpty().addKey('A', 2));
	}

	@Test
	public void testAddB3()
	{
		Assertions.assertEquals(mapA2B3(), mapA2().addKey('B', 3));
	}

	@Test
	public void testAddC1()
	{
		Assertions.assertEquals(mapA2B3C1(), mapA2B3().addKey('C', 1));
	}

	@Test
	public void testAddA4()
	{
		Assertions.assertEquals(mapA4B3C1(), mapA2B3C1().addKey('A', 4));
	}

	@Test
	public void testContainsKeyEmpty()
	{
		Assertions.assertFalse(mapEmpty().containsKey('A'));
		Assertions.assertFalse(mapEmpty().containsKey('B'));
		Assertions.assertFalse(mapEmpty().containsKey('C'));
	}

	@Test
	public void testContainsKeyA2()
	{
		Assertions.assertTrue(mapA2().containsKey('A'));
		Assertions.assertFalse(mapA2().containsKey('B'));
		Assertions.assertFalse(mapA2().containsKey('C'));
	}

	@Test
	public void testContainsKeyA2B3()
	{
		Assertions.assertTrue(mapA2B3().containsKey('A'));
		Assertions.assertTrue(mapA2B3().containsKey('B'));
		Assertions.assertFalse(mapA2B3().containsKey('C'));
	}

	@Test
	public void testContainsKeyA2B3C1()
	{
		Assertions.assertTrue(mapA2B3C1().containsKey('A'));
		Assertions.assertTrue(mapA2B3C1().containsKey('B'));
		Assertions.assertTrue(mapA2B3C1().containsKey('C'));
	}

	@Test
	public void testEqualsEmpty()
	{
		Assertions.assertTrue(mapEmpty().equals(mapEmpty()));
		Assertions.assertEquals(mapEmpty(), mapEmpty());
		Assertions.assertFalse(mapEmpty().equals(mapA2()));
		Assertions.assertNotEquals(mapEmpty(), mapA2());
		Assertions.assertFalse(mapEmpty().equals(mapA2B3()));
		Assertions.assertNotEquals(mapEmpty(), mapA2B3());
		Assertions.assertFalse(mapEmpty().equals(mapA2B3C1()));
		Assertions.assertNotEquals(mapEmpty(), mapA2B3C1());
		Assertions.assertFalse(mapEmpty().equals(mapA4B3C1()));
		Assertions.assertNotEquals(mapEmpty(), mapA4B3C1());
	}

	@Test
	public void testEqualsA2()
	{
		Assertions.assertFalse(mapA2().equals(mapEmpty()));
		Assertions.assertNotEquals(mapA2(), mapEmpty());
		Assertions.assertTrue(mapA2().equals(mapA2()));
		Assertions.assertEquals(mapA2(), mapA2());
		Assertions.assertFalse(mapA2().equals(mapA2B3()));
		Assertions.assertNotEquals(mapA2(), mapA2B3());
		Assertions.assertFalse(mapA2().equals(mapA2B3C1()));
		Assertions.assertNotEquals(mapA2(), mapA2B3C1());
		Assertions.assertFalse(mapA2().equals(mapA4B3C1()));
		Assertions.assertNotEquals(mapA2(), mapA4B3C1());
	}

	@Test
	public void testEqualsA2B3()
	{
		Assertions.assertFalse(mapA2B3().equals(mapEmpty()));
		Assertions.assertNotEquals(mapA2B3(), mapEmpty());
		Assertions.assertFalse(mapA2B3().equals(mapA2()));
		Assertions.assertNotEquals(mapA2B3(), mapA2());
		Assertions.assertTrue(mapA2B3().equals(mapA2B3()));
		Assertions.assertEquals(mapA2B3(), mapA2B3());
		Assertions.assertFalse(mapA2B3().equals(mapA2B3C1()));
		Assertions.assertNotEquals(mapA2B3(), mapA2B3C1());
		Assertions.assertFalse(mapA2B3().equals(mapA4B3C1()));
		Assertions.assertNotEquals(mapA2B3(), mapA4B3C1());
	}

	@Test
	public void testEqualsA2B3C1()
	{
		Assertions.assertFalse(mapA2B3C1().equals(mapEmpty()));
		Assertions.assertNotEquals(mapA2B3C1(), mapEmpty());
		Assertions.assertFalse(mapA2B3C1().equals(mapA2()));
		Assertions.assertNotEquals(mapA2B3C1(), mapA2());
		Assertions.assertFalse(mapA2B3C1().equals(mapA2B3()));
		Assertions.assertNotEquals(mapA2B3C1(), mapA2B3());
		Assertions.assertTrue(mapA2B3C1().equals(mapA2B3C1()));
		Assertions.assertEquals(mapA2B3C1(), mapA2B3C1());
		Assertions.assertFalse(mapA2B3C1().equals(mapA4B3C1()));
		Assertions.assertNotEquals(mapA2B3C1(), mapA4B3C1());
	}

	@Test
	public void testEqualsA4B3C1()
	{
		Assertions.assertFalse(mapA4B3C1().equals(mapEmpty()));
		Assertions.assertNotEquals(mapA4B3C1(), mapEmpty());
		Assertions.assertFalse(mapA4B3C1().equals(mapA2()));
		Assertions.assertNotEquals(mapA4B3C1(), mapA2());
		Assertions.assertFalse(mapA4B3C1().equals(mapA2B3()));
		Assertions.assertNotEquals(mapA4B3C1(), mapA2B3());
		Assertions.assertFalse(mapA4B3C1().equals(mapA2B3C1()));
		Assertions.assertNotEquals(mapA4B3C1(), mapA2B3C1());
		Assertions.assertTrue(mapA4B3C1().equals(mapA4B3C1()));
		Assertions.assertEquals(mapA4B3C1(), mapA4B3C1());
	}

	@Test
	public void testGetKeyEmpty()
	{
		Assertions.assertFalse(mapEmpty().getKey('A').isPresent());
		Assertions.assertFalse(mapEmpty().getKey('B').isPresent());
		Assertions.assertFalse(mapEmpty().getKey('C').isPresent());
	}

	@Test
	public void testGetKeyA2()
	{
		Assertions.assertTrue(mapA2().getKey('A').isPresent());
		Assertions.assertEquals(2, mapA2().getKey('A').get());
		Assertions.assertFalse(mapEmpty().getKey('B').isPresent());
		Assertions.assertFalse(mapEmpty().getKey('C').isPresent());
	}

	@Test
	public void testGetKeyA2B3()
	{
		Assertions.assertTrue(mapA2B3().getKey('A').isPresent());
		Assertions.assertEquals(2, mapA2B3().getKey('A').get());
		Assertions.assertTrue(mapA2B3().getKey('B').isPresent());
		Assertions.assertEquals(3, mapA2B3().getKey('B').get());
		Assertions.assertFalse(mapEmpty().getKey('C').isPresent());
	}

	@Test
	public void testGetKeyA2B3C1()
	{
		Assertions.assertTrue(mapA2B3C1().getKey('A').isPresent());
		Assertions.assertEquals(2, mapA2B3C1().getKey('A').get());
		Assertions.assertTrue(mapA2B3C1().getKey('B').isPresent());
		Assertions.assertEquals(3, mapA2B3C1().getKey('B').get());
		Assertions.assertTrue(mapA2B3C1().getKey('C').isPresent());
		Assertions.assertEquals(1, mapA2B3C1().getKey('C').get());
	}

	@Test
	public void testHeadMap()
	{
		Assertions.assertEquals
			(
				mapEmpty()
					.addKey('A', 2),
				mapA2B3C1()
					.headMap('B')
			);
	}

	@Test
	public void testKeysEmpty()
	{
		Iterator<Character> iterator = mapEmpty().keys().iterator();
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testKeysA2()
	{
		Iterator<Character> iterator = mapA2().keys().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testKeysA2B3()
	{
		Iterator<Character> iterator = mapA2B3().keys().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testKeysA2B3C1()
	{
		Iterator<Character> iterator = mapA2B3C1().keys().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('C', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testRemoveA2()
	{
		Assertions.assertEquals(mapEmpty(), mapA2().removeKey('A'));
	}

	@Test
	public void testRemoveB3()
	{
		Assertions.assertEquals(mapA2(), mapA2B3().removeKey('B'));
	}

	@Test
	public void testRemoveC1()
	{
		Assertions.assertEquals(mapA2B3(), mapA2B3C1().removeKey('C'));
	}

	@Test
	public void testTailMap()
	{
		Assertions.assertEquals
			(
				mapEmpty()
					.addKey('B', 3)
					.addKey('C', 1),
				mapA2B3C1()
					.tailMap('B')
			);
	}

	@Test
	public void testValuesEmpty()
	{
		Iterator<Integer> iterator = mapEmpty().values().iterator();
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testValuesA2()
	{
		Iterator<Integer> iterator = mapA2().values().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(2, iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testValuesA2B3()
	{
		Iterator<Integer> iterator = mapA2B3().values().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(2, iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(3, iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testValuesA2B3C1()
	{
		Iterator<Integer> iterator = mapA2B3C1().values().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(2, iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(3, iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals(1, iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}
}