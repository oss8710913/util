package nl.novit.util.collections.set.setavl;

import nl.novit.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.Iterator;

public class TestNode
{
	public static final Comparator<String> COMPARATOR_FIRST_CHARACTER = Comparator.comparingInt(x -> x.charAt(0));

	public static <Type> boolean isBalanced(Node<Type> node)
	{
		return node == null || (Math.abs(Node.height(node.left) - Node.height(node.right)) <= 1 && isBalanced(node.left) && isBalanced(node.right));
	}

	public static <Type> int compare(Comparator<? super Type> comparator, Node<Type> nodeA, Node<Type> nodeB)
	{
		return compareNode
			(
				new Comparator<>()
				{
					@Override
					public int compare(Node<Type> nodeA, Node<Type> nodeB)
					{
						int result = comparator.compare(nodeA.value, nodeB.value);
						if (result == 0)
						{
							result = compareNode(this, nodeA.left, nodeB.left);
							if (result == 0)
							{
								result = compareNode(this, nodeA.right, nodeB.right);
							}
						}
						return result;
					}
				},
				nodeA,
				nodeB
			);
	}

	public static <Type> int compareNode(Comparator<Node<Type>> comparator, Node<Type> nodeA, Node<Type> nodeB)
	{
		return Util.compareNullable(comparator, nodeA, nodeB);
	}

	public static <Type> boolean equals(Comparator<Type> comparator, Node<Type> nodeA, Node<Type> nodeB)
	{
		return compare(comparator, nodeA, nodeB) == 0;
	}

	public static <Type> Iterable<Type> iterable(final Node<Type> node)
	{
		return new Iterable<Type>()
		{
			@Override
			public Iterator<Type> iterator()
			{
				return new IteratorSet<>(node);
			}
		};
	}

	@Test
	public void testAdd1()
	{
		Node<Character> node =
			null;
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'C');
		Node<Character> expected =
			new Node<>
				(
					'C',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd2()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					null,
					null
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'C');
		Assertions.assertEquals(node, actual);
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd3()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					null,
					null
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'B');
		Node<Character> expected =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							null,
							null
						),
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd4()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					null,
					null
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'D');
		Node<Character> expected =
			new Node<>
				(
					'C',
					null,
					new Node<>
						(
							'D',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd5()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'A');
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd6()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'B');
		Assertions.assertEquals(node, actual);
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd7()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'B');
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd8()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							null,
							null
						),
					new Node<>
						(
							'D',
							null,
							new Node<>
								(
									'E',
									null,
									null
								)
						)
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'F');
		Node<Character> expected =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							null,
							null
						),
					new Node<>
						(
							'E',
							new Node<>
								(
									'D',
									null,
									null
								),
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testAdd9()
	{
		Node<Character> node =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							null,
							null
						),
					new Node<>
						(
							'E',
							new Node<>
								(
									'D',
									null,
									null
								),
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Node<Character> actual = Node.add(Comparator.naturalOrder(), node, 'G');
		Node<Character> expected =
			new Node<>
				(
					'E',
					new Node<>
						(
							'C',
							new Node<>
								(
									'B',
									null,
									null
								),
							new Node<>
								(
									'D',
									null,
									null
								)
						),
					new Node<>
						(
							'F',
							null,
							new Node<>
								(
									'G',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		Assertions.assertTrue(isBalanced(actual));
	}

	@Test
	public void testBalanceBalanced1()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					null
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'B',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testBalanceBalanced2()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testBalanceBalanced3()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'D',
							new Node<>
								(
									'C',
									null,
									null
								),
							new Node<>
								(
									'E',
									null,
									null
								)
						)
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'D',
							new Node<>
								(
									'C',
									null,
									null
								),
							new Node<>
								(
									'E',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testBalanceLeft()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'D',
							new Node<>
								(
									'C',
									null,
									null
								),
							new Node<>
								(
									'E',
									null,
									new Node<>
										(
											'F',
											null,
											null
										)
								)
						)
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'C',
									null,
									null
								)
						),
					new Node<>
						(
							'E',
							null,
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testBalanceRight()
	{
		Node<Character> node =
			new Node<>
				(
					'E',
					new Node<>
						(
							'C',
							new Node<>
								(
									'A',
									null,
									new Node<>
										(
											'B',
											null,
											null
										)
								),
							new Node<>
								(
									'D',
									null,
									null
								)
						),
					new Node<>
						(
							'F',
							null,
							null
						)
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'C',
					new Node<>
						(
							'A',
							null,
							new Node<>
								(
									'B',
									null,
									null
								)
						),
					new Node<>
						(
							'E',
							new Node<>
								(
									'D',
									null,
									null
								),
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testBalanceRightLeft()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'E',
							new Node<>
								(
									'C',
									null,
									new Node<>
										(
											'D',
											null,
											null
										)
								),
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'C',
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							null
						),
					new Node<>
						(
							'E',
							new Node<>
								(
									'D',
									null,
									null
								),
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testBalanceLeftRight()
	{
		Node<Character> node =
			new Node<>
				(
					'E',
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'D',
									new Node<>
										(
											'C',
											null,
											null
										),
									null
								)
						),
					new Node<>
						(
							'F',
							null,
							null
						)
				);
		Node<Character> actual = Node.balance(node);
		Node<Character> expected =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'C',
									null,
									null
								)
						),
					new Node<>
						(
							'E',
							null,
							new Node<>
								(
									'F',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testHeadSetSet1()
	{
		Node<Character> node =
			null;
		Assertions.assertNull(Node.headSet(Comparator.naturalOrder(), node, 'B'));
	}

	@Test
	public void testHeadSetSet2()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					null
				);
		Assertions.assertNull(Node.headSet(Comparator.naturalOrder(), node, 'B'));
	}

	@Test
	public void testHeadSetSet3()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					null
				);
		Node<Character> actual = Node.headSet(Comparator.naturalOrder(), node, 'C');
		Assertions.assertEquals(node, actual);
	}

	@Test
	public void testHeadSetSet4()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.headSet(Comparator.naturalOrder(), node, 'B');
		Assertions.assertEquals(node.left, actual);
	}

	@Test
	public void testHeadSetSet5()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> actual = Node.headSet(Comparator.naturalOrder(), node, 'C');
		Node<Character> expected =
			new Node<>
				(
					'B',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testHeadSetSet6()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> actual = Node.headSet(Comparator.naturalOrder(), node, 'C');
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testHeadSetSet7()
	{
		Node<Character> node =
			new Node<>
				(
					'H',
					new Node<>
						(
							'E',
							new Node<>
								(
									'C',
									new Node<>
										(
											'B',
											new Node<>
												(
													'A',
													null,
													null
												),
											null
										),
									new Node<>
										(
											'D',
											null,
											null
										)
								),
							new Node<>
								(
									'F',
									null,
									new Node<>
										(
											'G',
											null,
											null
										)
								)
						),
					new Node<>
						(
							'K',
							new Node<>
								(
									'J',
									new Node<>
										(
											'I',
											null,
											null
										),
									null
								),
							new Node<>
								(
									'M',
									new Node<>
										(
											'L',
											null,
											null
										),
									new Node<>
										(
											'N',
											null,
											new Node<>
												(
													'O',
													null,
													null
												)
										)
								)
						)
				);
		Node<Character> actual = Node.headSet(Comparator.naturalOrder(), node, 'L');
		Node<Character> expected =
			new Node<>
				(
					'E',
					new Node<>
						(
							'C',
							new Node<>
								(
									'B',
									new Node<>
										(
											'A',
											null,
											null
										),
									null
								),
							new Node<>
								(
									'D',
									null,
									null
								)
						),
					new Node<>
						(
							'H',
							new Node<>
								(
									'F',
									null,
									new Node<>
										(
											'G',
											null,
											null
										)
								),
							new Node<>
								(
									'J',
									new Node<>
										(
											'I',
											null,
											null
										),
									new Node<>
										(
											'K',
											null,
											null
										)
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testHeadSetSet8()
	{
		Node<Integer> node = null;
		for (int index = 0; index < 31; index++)
		{
			node = Node.add(Comparator.naturalOrder(), node, index);
		}
		node = Node.headSet(Comparator.naturalOrder(), node, 17);
		Assertions.assertFalse(isBalanced(node));
	}

	@Test
	public void testHeight()
	{
		Assertions.assertEquals
			(
				0,
				Node.height
					(
						null
					)
			);
		Assertions.assertEquals
			(
				1,
				Node.height
					(
						new Node<>
							(
								'A',
								null,
								null
							)
					)
			);
	}

	@Test
	public void testRemove1()
	{
		Node<Character> node =
			null;
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'E');
		Assertions.assertNull(actual);
		isBalanced(actual);
	}

	@Test
	public void testRemove2()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					null,
					null
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'D');
		Assertions.assertNull(actual);
		isBalanced(actual);
	}

	@Test
	public void testRemove3()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					null,
					null
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'A');
		Assertions.assertEquals(node, actual);
		isBalanced(actual);
	}

	@Test
	public void testRemove4()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'B');
		Node<Character> expected =
			new Node<>
				(
					'D',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		isBalanced(actual);
	}

	@Test
	public void testRemove5()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					null,
					new Node<>
						(
							'F',
							null,
							null
						)
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'F');
		Node<Character> expected =
			new Node<>
				(
					'D',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		isBalanced(actual);
	}

	@Test
	public void testRemove6()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'D');
		Node<Character> expected =
			new Node<>
				(
					'B',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		isBalanced(actual);
	}

	@Test
	public void testRemove7()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					null,
					new Node<>
						(
							'F',
							null,
							null
						)
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'D');
		Node<Character> expected =
			new Node<>
				(
					'F',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		isBalanced(actual);
	}

	@Test
	public void testRemove8()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							null,
							null
						),
					new Node<>
						(
							'F',
							null,
							null
						)
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'D');
		Node<Character> expected =
			new Node<>
				(
					'F',
					new Node<>
						(
							'B',
							null,
							null
						),
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		isBalanced(actual);
	}

	@Test
	public void testRemove9()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							null,
							null
						),
					new Node<>
						(
							'F',
							new Node<>
								(
									'E',
									null,
									null
								),
							null
						)
				);
		Node<Character> actual = Node.remove(Comparator.naturalOrder(), node, 'D');
		Node<Character> expected =
			new Node<>
				(
					'E',
					new Node<>
						(
							'B',
							null,
							null
						),
					new Node<>
						(
							'F',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
		isBalanced(actual);
	}

	@Test
	public void testTailSetSet1()
	{
		Node<Character> node =
			null;
		Assertions.assertNull(Node.tailSet(Comparator.naturalOrder(), node, 'B'));
	}

	@Test
	public void testTailSetSet2()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					null
				);
		Assertions.assertNull
			(
				Node.tailSet(Comparator.naturalOrder(), node, 'C')
			);
	}

	@Test
	public void testTailSetSet3()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					null
				);
		Node<Character> actual = Node.tailSet(Comparator.naturalOrder(), node, 'B');
		Assertions.assertEquals(node, actual);
	}

	@Test
	public void testTailSetSet4()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Node<Character> actual = Node.tailSet(Comparator.naturalOrder(), node, 'B');
		Node<Character> expected =
			new Node<>
				(
					'B',
					null,
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testTailSetSet5()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					null,
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> actual = Node.tailSet(Comparator.naturalOrder(), node, 'C');
		Assertions.assertTrue(equals(Comparator.naturalOrder(), node.right, actual));
	}

	@Test
	public void testTailSetSet6()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> actual = Node.tailSet(Comparator.naturalOrder(), node, 'B');
		Node<Character> expected =
			new Node<>
				(
					'B',
					null,
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testTailSetSet7()
	{
		Node<Character> node =
			new Node<>
				(
					'H',
					new Node<>
						(
							'E',
							new Node<>
								(
									'C',
									new Node<>
										(
											'B',
											new Node<>
												(
													'A',
													null,
													null
												),
											null
										),
									new Node<>
										(
											'D',
											null,
											null
										)
								),
							new Node<>
								(
									'F',
									null,
									new Node<>
										(
											'G',
											null,
											null
										)
								)
						),
					new Node<>
						(
							'K',
							new Node<>
								(
									'J',
									new Node<>
										(
											'I',
											null,
											null
										),
									null
								),
							new Node<>
								(
									'M',
									new Node<>
										(
											'L',
											null,
											null
										),
									new Node<>
										(
											'N',
											null,
											new Node<>
												(
													'O',
													null,
													null
												)
										)
								)
						)
				);
		Node<Character> actual = Node.tailSet(Comparator.naturalOrder(), node, 'E');
		Node<Character> expected =
			new Node<>
				(
					'K',
					new Node<>
						(
							'H',
							new Node<>
								(
									'F',
									new Node<>
										(
											'E',
											null,
											null
										),
									new Node<>
										(
											'G',
											null,
											null
										)
								),
							new Node<>
								(
									'J',
									new Node<>
										(
											'I',
											null,
											null
										),
									null
								)
						),
					new Node<>
						(
							'M',
							new Node<>
								(
									'L',
									null,
									null
								),
							new Node<>
								(
									'N',
									null,
									new Node<>
										(
											'O',
											null,
											null
										)
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testTailSetSet8()
	{
		Node<Integer> node = null;
		for (int index = 0; index < 31; index++)
		{
			node = Node.add(Comparator.naturalOrder(), node, index);
		}
		node = Node.tailSet(Comparator.naturalOrder(), node, 14);
		Assertions.assertFalse(isBalanced(node));
	}

	@Test
	public void testLeft1()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> left = null;
		Node<Character> actual = node.left(left);
		Node<Character> expected =
			new Node<>
				(
					'B',
					null,
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testLeft2()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> left =
			new Node<>
				(
					'A',
					null,
					null
				);
		Node<Character> actual = node.left(left);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), node, actual));
	}

	@Test
	public void testLeft3()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> actual = node.left(node.left);
		Assertions.assertEquals(node, actual);
	}

	@Test
	public void testRight1()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> right = null;
		Node<Character> actual = node.right(right);
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					null
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testRight2()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> right =
			new Node<>
				(
					'C',
					null,
					null
				);
		Node<Character> actual = node.right(right);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), node, actual));
	}

	@Test
	public void testRight3()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'C',
							null,
							null
						)
				);
		Node<Character> actual = node.right(node.right);
		Assertions.assertEquals(node, actual);
	}

	@Test
	public void testRotateLeft()
	{
		Node<Character> node =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'D',
							new Node<>
								(
									'C',
									null,
									null
								),
							new Node<>
								(
									'E',
									null,
									null
								)
						)
				);
		Node<Character> actual = node.rotateLeft();
		Node<Character> expected =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'C',
									null,
									null
								)
						),
					new Node<>
						(
							'E',
							null,
							null
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}

	@Test
	public void testRotateRight()
	{
		Node<Character> node =
			new Node<>
				(
					'D',
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'C',
									null,
									null
								)
						),
					new Node<>
						(
							'E',
							null,
							null
						)
				);
		Node<Character> actual = node.rotateRight();
		Node<Character> expected =
			new Node<>
				(
					'B',
					new Node<>
						(
							'A',
							null,
							null
						),
					new Node<>
						(
							'D',
							new Node<>
								(
									'C',
									null,
									null
								),
							new Node<>
								(
									'E',
									null,
									null
								)
						)
				);
		Assertions.assertTrue(equals(Comparator.naturalOrder(), expected, actual));
	}
}