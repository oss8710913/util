package nl.novit.util.collections.set.set23;

import nl.novit.util.collections.set.Set;
import nl.novit.util.collections.set.TestSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

public class TestSet23
	extends TestSet
{
	@Override
	public Set<Character> setEmpty()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				null
			);
	}

	@Override
	public Set<Character> setA()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				new NodeLeaf<>('A')
			);
	}

	@Override
	public Set<Character> setAB()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				new NodeInternal2<>
					(
						new NodeLeaf<>('A'),
						new NodeLeaf<>('B')
					)
			);
	}

	@Override
	public Set<Character> setABC()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				new NodeInternal3<>
					(
						new NodeLeaf<>('A'),
						new NodeLeaf<>('B'),
						new NodeLeaf<>('C')
					)
			);
	}

	@Override
	public Set<Character> setABCD()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				new NodeInternal2<>
					(
						new NodeInternal2<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B')
							),
						new NodeInternal2<>
							(
								new NodeLeaf<>('C'),
								new NodeLeaf<>('D')
							)
					)
			);
	}

	@Override
	public Set<Character> setABCDE()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				new NodeInternal2<>
					(
						new NodeInternal2<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B')
							),
						new NodeInternal3<>
							(
								new NodeLeaf<>('C'),
								new NodeLeaf<>('D'),
								new NodeLeaf<>('E')
							)
					)
			);
	}

	@Override
	public Set<Character> setABCDEF()
	{
		return new Set23<>
			(
				Comparator.naturalOrder(),
				new NodeInternal2<>
					(
						new NodeInternal2<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B')
							),
						new NodeInternal2<>
							(
								new NodeInternal2<>
									(
										new NodeLeaf<>('C'),
										new NodeLeaf<>('D')
									),
								new NodeInternal2<>
									(
										new NodeLeaf<>('E'),
										new NodeLeaf<>('F')
									)
							)
					)
			);
	}

	@Test
	public void testMerge1()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder()
					),
				new Set23<>
					(
						Comparator.naturalOrder()
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder()
								)
						)
			);
	}

	@Test
	public void testMerge2()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeLeaf<>('A')
					),
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeLeaf<>('A')
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder()
								)
						)
			);
	}

	@Test
	public void testMerge3()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeLeaf<>('A')
					),
				new Set23<Character>
					(
						Comparator.naturalOrder()
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder(),
									new NodeLeaf<>('A')
								)
						)
			);
	}

	@Test
	public void testMerge4()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal2<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B')
							)
					),
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeLeaf<>('A')
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder(),
									new NodeLeaf<>('B')
								)
						)
			);
	}

	@Test
	public void testMerge5()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal3<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B'),
								new NodeLeaf<>('C')
							)
					),
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal2<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B')
							)
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder(),
									new NodeLeaf<>('C')
								)
						)
			);
	}

	@Test
	public void testMerge6()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal2<>
							(
								new NodeInternal2<>
									(
										new NodeLeaf<>('A'),
										new NodeLeaf<>('B')
									),
								new NodeInternal2<>
									(
										new NodeLeaf<>('C'),
										new NodeLeaf<>('D')
									)
							)
					),
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal3<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B'),
								new NodeLeaf<>('C')
							)
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder(),
									new NodeLeaf<>('D')
								)
						)
			);
	}

	@Test
	public void testMerge7()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal2<>
							(
								new NodeInternal2<>
									(
										new NodeLeaf<>('A'),
										new NodeLeaf<>('B')
									),
								new NodeInternal3<>
									(
										new NodeLeaf<>('C'),
										new NodeLeaf<>('D'),
										new NodeLeaf<>('E')
									)
							)
					),
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal3<>
							(
								new NodeLeaf<>('A'),
								new NodeLeaf<>('B'),
								new NodeLeaf<>('C')
							)
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder(),
									new NodeInternal2<>
										(
											new NodeLeaf<>('D'),
											new NodeLeaf<>('E')
										)
								)
						)
			);
	}

	@Test
	public void testMerge8()
	{
		Assertions.assertEquals
			(
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal3<>
							(
								new NodeInternal2<>
									(
										new NodeLeaf<>('A'),
										new NodeLeaf<>('B')
									),
								new NodeInternal2<>
									(
										new NodeLeaf<>('C'),
										new NodeLeaf<>('D')
									),
								new NodeInternal2<>
									(
										new NodeLeaf<>('E'),
										new NodeLeaf<>('F')
									)
							)
					),
				new Set23<>
					(
						Comparator.naturalOrder(),
						new NodeInternal2<>
							(
								new NodeInternal2<>
									(
										new NodeLeaf<>('A'),
										new NodeLeaf<>('B')
									),
								new NodeInternal3<>
									(
										new NodeLeaf<>('C'),
										new NodeLeaf<>('D'),
										new NodeLeaf<>('E')
									)
							)
					)
					.merge
						(
							new Set23<>
								(
									Comparator.naturalOrder(),
									new NodeLeaf<>('F')
								)
						)
			);
	}
}