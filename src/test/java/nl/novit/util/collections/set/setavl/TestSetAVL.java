package nl.novit.util.collections.set.setavl;

import nl.novit.util.collections.set.Set;
import nl.novit.util.collections.set.TestSet;

import java.util.Comparator;

public class TestSetAVL
	extends TestSet
{
	@Override
	public Set<Character> setEmpty()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					null
				);
	}

	@Override
	public Set<Character> setA()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					new Node<>
						(
							'A',
							null,
							null
						)
				);
	}

	@Override
	public Set<Character> setAB()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					new Node<>
						(
							'A',
							null,
							new Node<>
								(
									'B',
									null,
									null
								)
						)
				);
	}

	@Override
	public Set<Character> setABC()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'C',
									null,
									null
								)
						)
				);
	}

	@Override
	public Set<Character> setABCD()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'C',
									null,
									new Node<>
										(
											'D',
											null,
											null
										)
								)
						)
				);
	}

	@Override
	public Set<Character> setABCDE()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					new Node<>
						(
							'B',
							new Node<>
								(
									'A',
									null,
									null
								),
							new Node<>
								(
									'D',
									new Node<>
										(
											'C',
											null,
											null
										),
									new Node<>
										(
											'E',
											null,
											null
										)
								)
						)
				);
	}

	@Override
	public Set<Character> setABCDEF()
	{
		return
			new SetAVL<>
				(
					Comparator.naturalOrder(),
					new Node<>
						(
							'C',
							new Node<>
								(
									'A',
									null,
									new Node<>
										(
											'B',
											null,
											null
										)
								),
							new Node<>
								(
									'E',
									new Node<>
										(
											'D',
											null,
											null
										),
									new Node<>
										(
											'F',
											null,
											null
										)
								)
						)
				);
	}
}