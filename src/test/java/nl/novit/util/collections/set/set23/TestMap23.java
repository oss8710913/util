package nl.novit.util.collections.set.set23;

import nl.novit.util.collections.set.Map;
import nl.novit.util.collections.set.MapDefault;
import nl.novit.util.collections.set.TestMap;
import nl.novit.util.collections.set.Tuple2Map;

import java.util.Comparator;

public class TestMap23
	extends TestMap
{
	@Override
	public Map<Character, Integer> mapEmpty()
	{
		return
			new Map23<>
				(
					Comparator.naturalOrder()
				);
	}

	@Override
	public Map<Character, Integer> mapA2()
	{
		return
			new Map23<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new NodeLeaf<>(new Tuple2Map<>('A', 2))
				);
	}

	@Override
	public Map<Character, Integer> mapA2B3()
	{
		return
			new Map23<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new NodeInternal2<>
						(
							new NodeLeaf<>(new Tuple2Map<>('A', 2)),
							new NodeLeaf<>(new Tuple2Map<>('B', 3))
						)
				);
	}

	@Override
	public Map<Character, Integer> mapA2B3C1()
	{
		return
			new Map23<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new NodeInternal3<>
						(
							new NodeLeaf<>(new Tuple2Map<>('A', 2)),
							new NodeLeaf<>(new Tuple2Map<>('B', 3)),
							new NodeLeaf<>(new Tuple2Map<>('C', 1))
						)
				);
	}

	@Override
	public Map<Character, Integer> mapA4B3C1()
	{
		return
			new Map23<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new NodeInternal3<>
						(
							new NodeLeaf<>(new Tuple2Map<>('A', 4)),
							new NodeLeaf<>(new Tuple2Map<>('B', 3)),
							new NodeLeaf<>(new Tuple2Map<>('C', 1))
						)
				);
	}
}
