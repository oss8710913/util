package nl.novit.util.collections.set.setavl;

import nl.novit.util.collections.set.Map;
import nl.novit.util.collections.set.MapDefault;
import nl.novit.util.collections.set.TestMap;
import nl.novit.util.collections.set.Tuple2Map;

import java.util.Comparator;

public class TestMapAVL
	extends TestMap
{
	@Override
	public Map<Character, Integer> mapEmpty()
	{
		return
			new MapAVL<>
				(
					Comparator.naturalOrder()
				);
	}

	@Override
	public Map<Character, Integer> mapA2()
	{
		return
			new MapAVL<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new Node<>
						(
							new Tuple2Map<>('A', 2),
							null,
							null
						)
				);
	}

	@Override
	public Map<Character, Integer> mapA2B3()
	{
		return
			new MapAVL<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new Node<>
						(
							new Tuple2Map<>('A', 2),
							null,
							new Node<>
								(
									new Tuple2Map<>('B', 3),
									null,
									null
								)
						)
				);
	}

	@Override
	public Map<Character, Integer> mapA2B3C1()
	{
		return
			new MapAVL<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new Node<>
						(
							new Tuple2Map<>('B', 3),
							new Node<>
								(
									new Tuple2Map<>('A', 2),
									null,
									null
								),
							new Node<>
								(
									new Tuple2Map<>('C', 1),
									null,
									null
								)
						)
				);
	}

	@Override
	public Map<Character, Integer> mapA4B3C1()
	{
		return
			new MapAVL<Character, Integer>
				(
					MapDefault.comparator(Comparator.naturalOrder()),
					new Node<>
						(
							new Tuple2Map<>('B', 3),
							new Node<>
								(
									new Tuple2Map<>('A', 4),
									null,
									null
								),
							new Node<>
								(
									new Tuple2Map<>('C', 1),
									null,
									null
								)
						)
				);
	}
}