package nl.novit.util.collections.set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public abstract class TestSet
{
	public abstract Set<Character> setEmpty();

	public abstract Set<Character> setA();

	public abstract Set<Character> setAB();

	public abstract Set<Character> setABC();

	public abstract Set<Character> setABCD();

	public abstract Set<Character> setABCDE();

	public abstract Set<Character> setABCDEF();

	@Test
	public void testAddA()
	{
		Assertions.assertEquals
			(
				setA(),
				setEmpty()
					.add('A')
			);
		Assertions.assertEquals
			(
				setA(),
				setA()
					.add('A')
			);
		Assertions.assertEquals
			(
				setAB(),
				setAB()
					.add('A')
			);
		Assertions.assertEquals
			(
				setABC(),
				setABC()
					.add('A')
			);
	}

	@Test
	public void testAddAB()
	{
		Assertions.assertEquals
			(
				setAB(),
				setEmpty()
					.add('A')
					.add('B')
			);
		Assertions.assertEquals
			(
				setAB(),
				setA()
					.add('A')
					.add('B')
			);
		Assertions.assertEquals
			(
				setAB(),
				setAB()
					.add('A')
					.add('B')
			);
		Assertions.assertEquals
			(
				setABC(),
				setABC()
					.add('A')
					.add('B')
			);
	}

	@Test
	public void testAddABC()
	{
		Assertions.assertEquals
			(
				setABC(),
				setEmpty()
					.add('A')
					.add('B')
					.add('C')
			);
		Assertions.assertEquals
			(
				setABC(),
				setA()
					.add('A')
					.add('B')
					.add('C')
			);
		Assertions.assertEquals
			(
				setABC(),
				setAB()
					.add('A')
					.add('B')
					.add('C')
			);
		Assertions.assertEquals
			(
				setABC(),
				setABC()
					.add('A')
					.add('B')
					.add('C')
			);
	}

	@Test
	public void testAddACB()
	{
		Assertions.assertEquals
			(
				setABC(),
				setEmpty()
					.add('A')
					.add('C')
					.add('B')
			);
	}

	@Test
	public void testAddBCEDAF()
	{
		Assertions.assertEquals
			(
				setABCDEF(),
				setEmpty()
					.add('B')
					.add('C')
					.add('E')
					.add('D')
					.add('A')
					.add('F')
			);
	}

	@Test
	public void testAddSetEmpty()
	{
		Assertions.assertEquals(setEmpty(), setEmpty().add(setEmpty()));
	}

	@Test
	public void testAddSetA()
	{
		Assertions.assertEquals(setA(), setEmpty().add(setA()));
	}

	@Test
	public void testAddSetDE()
	{
		Assertions.assertEquals
			(
				setABCDE(),
				setABC()
					.add
						(
							setEmpty()
								.add('D')
								.add('E')
						)
			);
	}

	@Test
	public void testCompareToEmpty()
	{
		Assertions.assertEquals(0, setEmpty().compareTo(setEmpty()));
		Assertions.assertEquals(-1, setEmpty().compareTo(setA()));
		Assertions.assertEquals(-1, setEmpty().compareTo(setAB()));
	}

	@Test
	public void testCompareToA()
	{
		Assertions.assertEquals(1, setA().compareTo(setEmpty()));
		Assertions.assertEquals(0, setA().compareTo(setA()));
		Assertions.assertEquals(-1, setA().compareTo(setAB()));
	}

	@Test
	public void testCompareToAB()
	{
		Assertions.assertEquals(1, setAB().compareTo(setEmpty()));
		Assertions.assertEquals(1, setAB().compareTo(setA()));
		Assertions.assertEquals(0, setAB().compareTo(setAB()));
	}

	@Test
	public void testContainsEmpty()
	{
		Assertions.assertFalse(setEmpty().contains('A'));
		Assertions.assertFalse(setEmpty().contains('B'));
		Assertions.assertFalse(setEmpty().contains('C'));
	}

	@Test
	public void testContainsA()
	{
		Assertions.assertTrue(setA().contains('A'));
		Assertions.assertFalse(setA().contains('B'));
		Assertions.assertFalse(setA().contains('C'));
	}

	@Test
	public void testContainsAB()
	{
		Assertions.assertTrue(setAB().contains('A'));
		Assertions.assertTrue(setAB().contains('B'));
		Assertions.assertFalse(setAB().contains('C'));
	}

	@Test
	public void testContainsABC()
	{
		Assertions.assertTrue(setABC().contains('A'));
		Assertions.assertTrue(setABC().contains('B'));
		Assertions.assertTrue(setABC().contains('C'));
	}

	@Test
	public void testContainsSetEmpty()
	{
		Assertions.assertTrue(setEmpty().contains(setEmpty()));
		Assertions.assertFalse(setEmpty().contains(setA()));
		Assertions.assertFalse(setEmpty().contains(setAB()));
		Assertions.assertFalse(setEmpty().contains(setABC()));
	}

	@Test
	public void testContainsSetA()
	{
		Assertions.assertTrue(setA().contains(setEmpty()));
		Assertions.assertTrue(setA().contains(setA()));
		Assertions.assertFalse(setA().contains(setAB()));
		Assertions.assertFalse(setA().contains(setABC()));
	}

	@Test
	public void testContainsSetAB()
	{
		Assertions.assertTrue(setAB().contains(setEmpty()));
		Assertions.assertTrue(setAB().contains(setA()));
		Assertions.assertTrue(setAB().contains(setAB()));
		Assertions.assertFalse(setAB().contains(setABC()));
	}

	@Test
	public void testContainsSetABC()
	{
		Assertions.assertTrue(setABC().contains(setEmpty()));
		Assertions.assertTrue(setABC().contains(setA()));
		Assertions.assertTrue(setABC().contains(setAB()));
		Assertions.assertTrue(setABC().contains(setABC()));
	}

	@Test
	public void testFirstEmpty()
	{
		Assertions.assertFalse(setEmpty().first().isPresent());
	}

	@Test
	public void testFirst()
	{
		Assertions.assertTrue(setA().first().isPresent());
		Assertions.assertEquals('A', setA().first().get());
		Assertions.assertTrue(setAB().first().isPresent());
		Assertions.assertEquals('A', setAB().first().get());
		Assertions.assertTrue(setABC().first().isPresent());
		Assertions.assertEquals('A', setABC().first().get());
		Assertions.assertTrue(setABCD().first().isPresent());
		Assertions.assertEquals('A', setABCD().first().get());
		Assertions.assertTrue(setABCDE().first().isPresent());
		Assertions.assertEquals('A', setABCDE().first().get());
		Assertions.assertTrue(setABCDEF().first().isPresent());
		Assertions.assertEquals('A', setABCDEF().first().get());
	}

	@Test
	public void testHeadSetEmpty()
	{
		Assertions.assertEquals(setEmpty(), setEmpty().headSet('A'));
	}

	@Test
	public void testHeadSetA()
	{
		Assertions.assertEquals(setEmpty(), setA().headSet('A'));
		Assertions.assertEquals(setA(), setA().headSet('B'));
	}

	@Test
	public void testHeadSetAB()
	{
		Assertions.assertEquals(setEmpty(), setAB().headSet('A'));
		Assertions.assertEquals(setA(), setAB().headSet('B'));
		Assertions.assertEquals(setAB(), setAB().headSet('C'));
	}

	@Test
	public void testHeadSetABC()
	{
		Assertions.assertEquals(setEmpty(), setABC().headSet('A'));
		Assertions.assertEquals(setA(), setABC().headSet('B'));
		Assertions.assertEquals(setAB(), setABC().headSet('C'));
		Assertions.assertEquals(setABC(), setABC().headSet('D'));
	}

	@Test
	public void testHeadSetABCD()
	{
		Assertions.assertEquals(setEmpty(), setABCD().headSet('A'));
		Assertions.assertEquals(setA(), setABCD().headSet('B'));
		Assertions.assertEquals(setAB(), setABCD().headSet('C'));
		Assertions.assertEquals(setABC(), setABCD().headSet('D'));
		Assertions.assertEquals(setABCD(), setABCD().headSet('E'));
	}

	@Test
	public void testHeadSetABCDE()
	{
		Assertions.assertEquals(setEmpty(), setABCDE().headSet('A'));
		Assertions.assertEquals(setA(), setABCDE().headSet('B'));
		Assertions.assertEquals(setAB(), setABCDE().headSet('C'));
		Assertions.assertEquals(setABC(), setABCDE().headSet('D'));
		Assertions.assertEquals(setABCD(), setABCDE().headSet('E'));
		Assertions.assertEquals(setABCDE(), setABCDE().headSet('F'));
	}

	@Test
	public void testHeadSetABCDEF()
	{
		Assertions.assertEquals(setEmpty(), setABCDEF().headSet('A'));
		Assertions.assertEquals(setA(), setABCDEF().headSet('B'));
		Assertions.assertEquals(setAB(), setABCDEF().headSet('C'));
		Assertions.assertEquals(setABC(), setABCDEF().headSet('D'));
		Assertions.assertEquals(setABCD(), setABCDEF().headSet('E'));
		Assertions.assertEquals(setABCDE(), setABCDEF().headSet('F'));
		Assertions.assertEquals(setABCDEF(), setABCDEF().headSet('G'));
	}

	@Test
	public void testIsEmpty()
	{
		Assertions.assertTrue(setEmpty().isEmpty());
		Assertions.assertFalse(setA().isEmpty());
	}

	@Test
	public void testIteratorEmpty()
	{
		Iterator<Character> iterator = setEmpty().iterator();
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorA()
	{
		Iterator<Character> iterator = setA().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorAB()
	{
		Iterator<Character> iterator = setAB().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorABC()
	{
		Iterator<Character> iterator = setABC().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('C', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorABCD()
	{
		Iterator<Character> iterator = setABCD().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('C', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('D', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorABCDE()
	{
		Iterator<Character> iterator = setABCDE().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('C', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('D', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('E', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testIteratorABCDEF()
	{
		Iterator<Character> iterator = setABCDEF().iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('A', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('B', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('C', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('D', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('E', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('F', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testLastEmpty()
	{
		Assertions.assertFalse(setEmpty().last().isPresent());
	}

	@Test
	public void testLast()
	{
		Assertions.assertTrue(setA().last().isPresent());
		Assertions.assertEquals('A', setA().last().get());
		Assertions.assertTrue(setAB().last().isPresent());
		Assertions.assertEquals('B', setAB().last().get());
		Assertions.assertTrue(setABC().last().isPresent());
		Assertions.assertEquals('C', setABC().last().get());
		Assertions.assertTrue(setABCD().last().isPresent());
		Assertions.assertEquals('D', setABCD().last().get());
		Assertions.assertTrue(setABCDE().last().isPresent());
		Assertions.assertEquals('E', setABCDE().last().get());
		Assertions.assertTrue(setABCDEF().last().isPresent());
		Assertions.assertEquals('F', setABCDEF().last().get());
	}

	@Test
	public void testRemoveEmpty()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setEmpty()
					.remove('A')
			);
	}

	@Test
	public void testRemoveA()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setA()
					.remove('A')
			);
	}

	@Test
	public void testRemoveE()
	{
		Assertions.assertEquals
			(
				setABCD()
					.add('F'),
				setABCDEF()
					.remove('E')
			);
	}

	@Test
	public void testRemoveDE()
	{
		Assertions.assertEquals
			(
				setABC()
					.add('F'),
				setABCDEF()
					.remove('D')
					.remove('E')
			);
	}

	@Test
	public void testRemoveED()
	{
		Assertions.assertEquals
			(
				setABC()
					.add('F'),
				setABCDEF()
					.remove('E')
					.remove('D')
			);
	}

	@Test
	public void testRemoveSetEmpty()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setEmpty()
					.remove
						(
							setEmpty()
						)
			);
	}

	@Test
	public void testRemoveSetA()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setA()
					.remove
						(
							setA()
						)
			);
	}

	@Test
	public void testRemoveSetDEF()
	{
		Assertions.assertEquals
			(
				setABC(),
				setABCDEF()
					.remove
						(
							setEmpty()
								.add('D')
								.add('E')
								.add('F')
						)
			);
	}

	@Test
	public void testSizeEmpty()
	{
		Assertions.assertEquals(0, setEmpty().size());
	}

	@Test
	public void testSizeA()
	{
		Assertions.assertEquals(1, setA().size());
	}

	@Test
	public void testSizeAB()
	{
		Assertions.assertEquals(2, setAB().size());
	}

	@Test
	public void testSizeABC()
	{
		Assertions.assertEquals(3, setABC().size());
	}

	@Test
	public void testSizeABCD()
	{
		Assertions.assertEquals(4, setABCD().size());
	}

	@Test
	public void testSizeABCDE()
	{
		Assertions.assertEquals(5, setABCDE().size());
	}

	@Test
	public void testSizeABCDF()
	{
		Assertions.assertEquals(6, setABCDEF().size());
	}

	@Test
	public void testTailSetEmpty()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setEmpty()
					.tailSet('A')
			);
	}

	@Test
	public void testTailSetA()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setA()
					.tailSet('B')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('A'),
				setA()
					.tailSet('A')
			);
	}

	@Test
	public void testTailSetAB()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setAB()
					.tailSet('C')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('B'),
				setAB()
					.tailSet('B')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('B')
					.add('A'),
				setAB()
					.tailSet('A')
			);
	}

	@Test
	public void testTailSetABC()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setABC()
					.tailSet('D')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('C'),
				setABC()
					.tailSet('C')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('C')
					.add('B'),
				setABC()
					.tailSet('B')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('C')
					.add('B')
					.add('A'),
				setABC()
					.tailSet('A')
			);
	}

	@Test
	public void testTailSetABCD()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setABCD()
					.tailSet('E')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('D'),
				setABCD()
					.tailSet('D')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('D')
					.add('C'),
				setABCD()
					.tailSet('C')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('D')
					.add('C')
					.add('B'),
				setABCD()
					.tailSet('B')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('D')
					.add('C')
					.add('B')
					.add('A'),
				setABCD()
					.tailSet('A')
			);
	}

	@Test
	public void testTailSetABCDE()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setABCDE()
					.tailSet('F')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('E'),
				setABCDE()
					.tailSet('E')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('E')
					.add('D'),
				setABCDE()
					.tailSet('D')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('E')
					.add('D')
					.add('C'),
				setABCDE()
					.tailSet('C')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('E')
					.add('D')
					.add('C')
					.add('B'),
				setABCDE()
					.tailSet('B')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('E')
					.add('D')
					.add('C')
					.add('B')
					.add('A'),
				setABCDE()
					.tailSet('A')
			);
	}

	@Test
	public void testTailSetABCDEF()
	{
		Assertions.assertEquals
			(
				setEmpty(),
				setABCDEF()
					.tailSet('G')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('F'),
				setABCDEF()
					.tailSet('F')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('F')
					.add('E'),
				setABCDEF()
					.tailSet('E')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('F')
					.add('E')
					.add('D'),
				setABCDEF()
					.tailSet('D')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('F')
					.add('E')
					.add('D')
					.add('C'),
				setABCDEF()
					.tailSet('C')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('F')
					.add('E')
					.add('D')
					.add('C')
					.add('B'),
				setABCDEF()
					.tailSet('B')
			);
		Assertions.assertEquals
			(
				setEmpty()
					.add('F')
					.add('E')
					.add('D')
					.add('C')
					.add('B')
					.add('A'),
				setABCDEF()
					.tailSet('A')
			);
	}
}