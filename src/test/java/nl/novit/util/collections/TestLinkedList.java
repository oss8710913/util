package nl.novit.util.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class TestLinkedList
{
	public static final ImmutableLinkedList<Character> LINKED_LIST_EMPTY = new ImmutableLinkedList<>();
	public static final ImmutableLinkedList<Character> LINKED_LIST_A = LINKED_LIST_EMPTY.addFirst('a');
	public static final ImmutableLinkedList<Character> LINKED_LIST_B = LINKED_LIST_EMPTY.addFirst('b');
	public static final ImmutableLinkedList<Character> LINKED_LIST_A_B = LINKED_LIST_B.addFirst('a');
	public static final ImmutableLinkedList<Character> LINKED_LIST_A_A_B = LINKED_LIST_A_B.addFirst('a');

	@Test
	public void testLinkedList()
	{
		Assertions.assertEquals(LINKED_LIST_EMPTY, new ImmutableLinkedList<>(List.of()));
		Assertions.assertEquals(LINKED_LIST_B, new ImmutableLinkedList<>(List.of('b')));
		Assertions.assertEquals(LINKED_LIST_A_B, new ImmutableLinkedList<>(List.of('a', 'b')));
	}

	@Test
	public void testIsEmpty()
	{
		Assertions.assertTrue(LINKED_LIST_EMPTY.isEmpty());
		Assertions.assertFalse(LINKED_LIST_B.isEmpty());
		Assertions.assertFalse(LINKED_LIST_A_B.isEmpty());
	}

	@Test
	public void testSize()
	{
		Assertions.assertEquals(0, LINKED_LIST_EMPTY.size());
		Assertions.assertEquals(1, LINKED_LIST_B.size());
		Assertions.assertEquals(2, LINKED_LIST_A_B.size());
	}

	@Test
	public void testContains()
	{
		Assertions.assertFalse(LINKED_LIST_EMPTY.contains('a'));
		Assertions.assertFalse(LINKED_LIST_EMPTY.contains('b'));
		Assertions.assertFalse(LINKED_LIST_B.contains('a'));
		Assertions.assertTrue(LINKED_LIST_B.contains('b'));
		Assertions.assertTrue(LINKED_LIST_A_B.contains('a'));
		Assertions.assertTrue(LINKED_LIST_A_B.contains('b'));
	}

	@Test
	public void testIterator()
	{
		Iterator<Character> iterator;
		iterator = LINKED_LIST_EMPTY.iterator();
		Assertions.assertFalse(iterator.hasNext());
		iterator = LINKED_LIST_B.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('b', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
		iterator = LINKED_LIST_A_B.iterator();
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('a', iterator.next());
		Assertions.assertTrue(iterator.hasNext());
		Assertions.assertEquals('b', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
		Assertions.assertThrows
			(
				NoSuchElementException.class,
				() -> LINKED_LIST_EMPTY.iterator().next()
			);
	}

	@Test
	public void testToArray()
	{
		Assertions.assertArrayEquals(new Object[] {}, LINKED_LIST_EMPTY.toArray());
		Assertions.assertArrayEquals(new Object[] {'b'}, LINKED_LIST_B.toArray());
		Assertions.assertArrayEquals(new Object[] {'a', 'b'}, LINKED_LIST_A_B.toArray());
		Assertions.assertArrayEquals(new Character[] {}, LINKED_LIST_EMPTY.toArray(new Character[] {}));
		Assertions.assertArrayEquals(new Character[] {'b'}, LINKED_LIST_B.toArray(new Character[] {}));
		Assertions.assertArrayEquals(new Character[] {'b'}, LINKED_LIST_B.toArray(new Character[] {'a'}));
		Assertions.assertArrayEquals(new Character[] {'b', null}, LINKED_LIST_B.toArray(new Character[] {'a', 'b'}));
		Assertions.assertArrayEquals(new Character[] {'a', 'b'}, LINKED_LIST_A_B.toArray(new Character[] {}));
	}

	@Test
	public void testContainsAll()
	{
		Assertions.assertTrue(LINKED_LIST_EMPTY.containsAll(List.of()));
		Assertions.assertFalse(LINKED_LIST_EMPTY.contains('a'));
		Assertions.assertFalse(LINKED_LIST_EMPTY.contains('b'));
		Assertions.assertFalse(LINKED_LIST_EMPTY.containsAll(List.of('a', 'b')));
		Assertions.assertTrue(LINKED_LIST_B.containsAll(List.of()));
		Assertions.assertFalse(LINKED_LIST_B.contains('a'));
		Assertions.assertTrue(LINKED_LIST_B.contains('b'));
		Assertions.assertFalse(LINKED_LIST_B.containsAll(List.of('a', 'b')));
		Assertions.assertTrue(LINKED_LIST_A_B.containsAll(List.of()));
		Assertions.assertTrue(LINKED_LIST_A_B.contains('a'));
		Assertions.assertTrue(LINKED_LIST_A_B.contains('b'));
		Assertions.assertTrue(LINKED_LIST_A_B.containsAll(List.of('a', 'b')));
	}

	@Test
	public void testGet()
	{
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.get(-1);
				}
			);
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.get(0);
				}
			);
		Assertions.assertEquals('b', LINKED_LIST_B.get(0));
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_B.get(1);
				}
			);
		Assertions.assertEquals('a', LINKED_LIST_A_B.get(0));
		Assertions.assertEquals('b', LINKED_LIST_A_B.get(1));
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_A_B.get(2);
				}
			);
	}

	@Test
	public void testIndexOf()
	{
		Assertions.assertEquals(-1, LINKED_LIST_EMPTY.indexOf('a'));
		Assertions.assertEquals(-1, LINKED_LIST_EMPTY.indexOf('b'));
		Assertions.assertEquals(-1, LINKED_LIST_B.indexOf('a'));
		Assertions.assertEquals(0, LINKED_LIST_B.indexOf('b'));
		Assertions.assertEquals(0, LINKED_LIST_A_B.indexOf('a'));
		Assertions.assertEquals(1, LINKED_LIST_A_B.indexOf('b'));
		Assertions.assertEquals(0, LINKED_LIST_A_A_B.indexOf('a'));
		Assertions.assertEquals(2, LINKED_LIST_A_A_B.indexOf('b'));
	}

	@Test
	public void testLastIndexOf()
	{
		Assertions.assertEquals(-1, LINKED_LIST_EMPTY.lastIndexOf('a'));
		Assertions.assertEquals(-1, LINKED_LIST_EMPTY.lastIndexOf('b'));
		Assertions.assertEquals(-1, LINKED_LIST_B.lastIndexOf('a'));
		Assertions.assertEquals(0, LINKED_LIST_B.lastIndexOf('b'));
		Assertions.assertEquals(0, LINKED_LIST_A_B.lastIndexOf('a'));
		Assertions.assertEquals(1, LINKED_LIST_A_B.lastIndexOf('b'));
		Assertions.assertEquals(1, LINKED_LIST_A_A_B.lastIndexOf('a'));
		Assertions.assertEquals(2, LINKED_LIST_A_A_B.lastIndexOf('b'));
	}

	public void testListIteratorEmpty(final ListIterator<Character> iterator)
	{
		{
			Assertions.assertFalse(iterator.hasNext());
			Assertions.assertThrows
				(
					NoSuchElementException.class,
					iterator::next
				);
		}
	}

	public void testListIteratorB(final ListIterator<Character> iterator)
	{
		{
			Assertions.assertTrue(iterator.hasNext());
			Assertions.assertEquals('b', iterator.next());
			testListIteratorEmpty(iterator);
		}
	}

	public void testListIteratorAB(final ListIterator<Character> iterator)
	{
		{
			Assertions.assertTrue(iterator.hasNext());
			Assertions.assertEquals('a', iterator.next());
			testListIteratorB(iterator);
		}
	}

	@Test
	public void testListIterator()
	{
		testListIteratorEmpty(LINKED_LIST_EMPTY.listIterator(0));
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() -> LINKED_LIST_EMPTY.listIterator(-1)
			);
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() -> LINKED_LIST_EMPTY.listIterator(1)
			);
		testListIteratorB(LINKED_LIST_B.listIterator(0));
		testListIteratorEmpty(LINKED_LIST_B.listIterator(1));
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() -> LINKED_LIST_B.listIterator(-1)
			);
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() -> LINKED_LIST_B.listIterator(2)
			);
		testListIteratorAB(LINKED_LIST_A_B.listIterator(0));
		testListIteratorB(LINKED_LIST_A_B.listIterator(1));
		testListIteratorEmpty(LINKED_LIST_A_B.listIterator(2));
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() -> LINKED_LIST_A_B.listIterator(-1)
			);
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() -> LINKED_LIST_A_B.listIterator(3)
			);
	}

	@Test
	public void testSubList()
	{
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.subList(-1, 0);
				}
			);
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.subList(1, 0);
				}
			);
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.subList(1, 1);
				}
			);
		Assertions.assertEquals(LINKED_LIST_EMPTY, LINKED_LIST_EMPTY.subList(0, 0));
		Assertions.assertEquals(LINKED_LIST_EMPTY, LINKED_LIST_B.subList(0, 0));
		Assertions.assertEquals(LINKED_LIST_B, LINKED_LIST_B.subList(0, 1));
		Assertions.assertEquals(LINKED_LIST_EMPTY, LINKED_LIST_A_B.subList(0, 0));
		Assertions.assertEquals(LINKED_LIST_A, LINKED_LIST_A_B.subList(0, 1));
		Assertions.assertEquals(LINKED_LIST_A_B, LINKED_LIST_A_B.subList(0, 2));
		Assertions.assertEquals(LINKED_LIST_B, LINKED_LIST_A_B.subList(1, 2));
	}

	@Test
	public void testEquals()
	{
		Assertions.assertTrue(LINKED_LIST_EMPTY.equals(LINKED_LIST_EMPTY));
		Assertions.assertFalse(LINKED_LIST_EMPTY.equals(LINKED_LIST_B));
		Assertions.assertFalse(LINKED_LIST_EMPTY.equals(LINKED_LIST_A_B));
		Assertions.assertFalse(LINKED_LIST_B.equals(LINKED_LIST_EMPTY));
		Assertions.assertTrue(LINKED_LIST_B.equals(LINKED_LIST_B));
		Assertions.assertFalse(LINKED_LIST_B.equals(LINKED_LIST_A_B));
		Assertions.assertFalse(LINKED_LIST_A_B.equals(LINKED_LIST_EMPTY));
		Assertions.assertFalse(LINKED_LIST_A_B.equals(LINKED_LIST_B));
		Assertions.assertTrue(LINKED_LIST_A_B.equals(LINKED_LIST_A_B));
		Assertions.assertTrue(new ImmutableLinkedList<>().addFirst('a').equals(new ImmutableLinkedList<>().addFirst('a')));
		Assertions.assertFalse(new ImmutableLinkedList<>().addFirst('a').equals(new ImmutableLinkedList<>().addFirst('b')));
	}

	@Test
	public void testFirst()
	{
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.first();
				}
			);
		Assertions.assertEquals('b', LINKED_LIST_B.first());
		Assertions.assertEquals('a', LINKED_LIST_A_B.first());
	}

	@Test
	public void testAddFirst()
	{
		ImmutableLinkedList<Character> list = new ImmutableLinkedList<>();
		Iterator<Character> iterator = list.iterator();
		Assertions.assertFalse(iterator.hasNext());
		list = list.addFirst('b');
		iterator = list.iterator();
		Assertions.assertEquals('b', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
		list = list.addFirst('a');
		iterator = list.iterator();
		Assertions.assertEquals('a', iterator.next());
		Assertions.assertEquals('b', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
	}

	@Test
	public void testRemoveFirst()
	{
		Assertions.assertThrows
			(
				IndexOutOfBoundsException.class,
				() ->
				{
					LINKED_LIST_EMPTY.removeFirst();
				}
			);
		ImmutableLinkedList<Character> list = new ImmutableLinkedList<Character>().addFirst('b').addFirst('a');
		Iterator<Character> iterator = list.iterator();
		Assertions.assertEquals('a', iterator.next());
		Assertions.assertEquals('b', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
		list = list.removeFirst();
		iterator = list.iterator();
		Assertions.assertEquals('b', iterator.next());
		Assertions.assertFalse(iterator.hasNext());
		list = list.removeFirst();
		iterator = list.iterator();
		Assertions.assertFalse(iterator.hasNext());
	}
}