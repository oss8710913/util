package nl.novit.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestRelation
{
	@Test
	public void testEmpty()
	{
		Relation<Character, Integer> relation = new Relation<>();
		Assertions.assertFalse(relation.allFirst().iterator().hasNext());
		Assertions.assertFalse(relation.allSecond().iterator().hasNext());
		Assertions.assertTrue(relation.getByFirst('a').isEmpty());
		Assertions.assertTrue(relation.getBySecond(1).isEmpty());
	}

	@Test
	public void test1()
	{
		Relation<Character, Integer> relation = new Relation<>();
		relation.add('a', 1);
		Assertions.assertEquals(0, Util.compareIterable(List.of('a'), relation.allFirst()));
		Assertions.assertEquals(0, Util.compareIterable(List.of(1), relation.allSecond()));
		Assertions.assertTrue(relation.getByFirst('b').isEmpty());
		Assertions.assertTrue(relation.getBySecond(2).isEmpty());
		Assertions.assertEquals(0, Util.compareIterable(List.of(1), relation.getByFirst('a')));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a'), relation.getBySecond(1)));
	}

	@Test
	public void test2()
	{
		Relation<Character, Integer> relation = new Relation<>();
		relation.add('a', 1);
		relation.add('b', 1);
		relation.add('b', 2);
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b'), relation.allFirst()));
		Assertions.assertEquals(0, Util.compareIterable(List.of(1, 2), relation.allSecond()));
		Assertions.assertEquals(0, Util.compareIterable(List.of(1), relation.getByFirst('a')));
		Assertions.assertEquals(0, Util.compareIterable(List.of(1, 2), relation.getByFirst('b')));
		Assertions.assertEquals(0, Util.compareIterable(List.of('a', 'b'), relation.getBySecond(1)));
		Assertions.assertEquals(0, Util.compareIterable(List.of('b'), relation.getBySecond(2)));
	}
}