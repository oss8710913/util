package nl.novit.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class Util
{
	public static <TypeClass> String getCanonicalName(Class<TypeClass> clazz)
	{
		String result = clazz.getCanonicalName();
		if (result == null)
		{
			result = getCanonicalName(clazz.getSuperclass());
		}
		return result;
	}

	public static boolean equalsNullable(Object a, Object b)
	{
		boolean result;
		if (a == null)
		{
			result = b == null;
		}
		else
		{
			if (b == null)
			{
				result = false;
			}
			else
			{
				result = a.equals(b);
			}
		}
		return result;
	}

	public static <Type extends Comparable<? super Type>> int compareNullable(Type a, Type b)
	{
		int result;
		if (a == null)
		{
			if (b == null)
			{
				result = 0;
			}
			else
			{
				result = -1;
			}
		}
		else
		{
			if (b == null)
			{
				result = 1;
			}
			else
			{
				result = a.compareTo(b);
			}
		}
		return result;
	}

	public static <Type> int compareNullable(Comparator<? super Type> comparator, Type a, Type b)
	{
		int result;
		if (a == null)
		{
			if (b == null)
			{
				result = 0;
			}
			else
			{
				result = -1;
			}
		}
		else
		{
			if (b == null)
			{
				result = 1;
			}
			else
			{
				result = comparator.compare(a, b);
			}
		}
		return result;
	}

	public static <Type> int compareIterable(Comparator<Type> comparator, Iterable<? extends Type> a, Iterable<? extends Type> b)
	{
		int result = 0;
		Iterator<? extends Type> iteratorA = a.iterator();
		Iterator<? extends Type> iteratorB = b.iterator();
		while (result == 0 && iteratorA.hasNext() && iteratorB.hasNext())
		{
			result = comparator.compare(iteratorA.next(), iteratorB.next());
		}
		if (result == 0)
		{
			if (iteratorA.hasNext())
			{
				result = 1;
			}
			if (iteratorB.hasNext())
			{
				result = -1;
			}
		}
		return result;
	}

	public static <Type extends Comparable<? super Type>> int compareIterable(Iterable<? extends Type> a, Iterable<? extends Type> b)
	{
		return compareIterable(Comparator.naturalOrder(), a, b);
	}

	public static <Type> boolean equalsIterable(Iterable<? extends Type> a, Iterable<? extends Type> b)
	{
		Comparator<Type> comparator = (x0, x1) -> equalsNullable(x0, x1) ? 0 : 1;
		return compareIterable(comparator, a, b) == 0;
	}

	public static List<Character> convert(String string)
	{
		List<Character> result = new Vector<>();
		for (int index = 0; index < string.length(); index++)
		{
			result.add(string.charAt(index));
		}
		return result;
	}

	public static String convert(List<Character> characters)
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (Character character: characters)
		{
			stringBuilder.append(character);
		}
		return stringBuilder.toString();
	}

	public static boolean and(boolean valueA, boolean valueB)
	{
		return valueA && valueB;
	}

	public static boolean or(boolean valueA, boolean valueB)
	{
		return valueA || valueB;
	}

	public static <Type> Stream<Type> progress
		(
			Stream<Type> stream,
			long size,
			int steps,
			Consumer<Byte> consumer
		)
	{
		final Wrapper<Long> count = new Wrapper<>(0L);
		return stream
			.peek
				(
					x ->
					{
						count.value += 1;
						if (steps * count.value / size != steps * (count.value - 1) / size)
						{
							consumer.accept((byte) (100 * count.value / size));
						}
					}
				);
	}

	public static <Type> String toString(Iterable<Type> iterable, String separator, Function1<String, ? super Type> toString)
	{
		StringBuilder stringBuilder = new StringBuilder();
		Iterator<Type> iterator = iterable.iterator();
		if (iterator.hasNext())
		{
			stringBuilder.append(toString.apply(iterator.next()));
			while (iterator.hasNext())
			{
				stringBuilder.append(separator);
				stringBuilder.append(toString.apply(iterator.next()));
			}
		}
		return stringBuilder.toString();
	}

	public static <Type> String toString(Iterable<Type> iterable, String separator)
	{
		return toString(iterable, separator, Object::toString);
	}

	public static <Type> String toString(Iterable<Type> iterable, char separator)
	{
		return toString(iterable, String.valueOf(separator));
	}

	public static <Type> String toString(Iterable<Type> iterable, String separator, char start, char end)
	{
		return start + toString(iterable, separator) + end;
	}

	public static <Type> String toString(Iterable<Type> iterable)
	{
		return toString(iterable, "", Object::toString);
	}
}