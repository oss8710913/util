package nl.novit.util;

public interface Function3Exception<TypeTarget, Type0, Type1, Type2>
{
	TypeTarget apply(Type0 value0, Type1 value1, Type2 value2)
		throws Exception;
}