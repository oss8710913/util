package nl.novit.util;

public interface Function1Exception<TypeTarget, Type0>
{
	TypeTarget apply(Type0 value0)
		throws Exception;
}