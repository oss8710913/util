package nl.novit.util;

public class Tuple2<Type0, Type1>
{
	public final Type0 value0;
	public final Type1 value1;

	public Tuple2(Type0 value0, Type1 value1)
	{
		this.value0 = value0;
		this.value1 = value1;
	}

	@Override
	public boolean equals(Object that)
	{
		boolean result = that instanceof Tuple2;
		if (result)
		{
			Tuple2<?, ?> tuple = (Tuple2<?, ?>) that;
			result &= this.value0.equals(tuple.value0);
			result &= this.value1.equals(tuple.value1);
		}
		return result;
	}

	public String toString()
	{
		return "(" + this.value0.toString() + ", " + this.value1.toString() + ")";
	}
}