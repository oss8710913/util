package nl.novit.util;

public class Tuple3<Type0, Type1, Type2>
{
	public final Type0 value0;
	public final Type1 value1;
	public final Type2 value2;

	public Tuple3(Type0 value0, Type1 value1, Type2 value2)
	{
		this.value0 = value0;
		this.value1 = value1;
		this.value2 = value2;
	}

	@Override
	public boolean equals(Object that)
	{
		boolean result = that instanceof Tuple3;
		if (result)
		{
			Tuple3 tuple = (Tuple3) that;
			result &= this.value0.equals(tuple.value0);
			result &= this.value1.equals(tuple.value1);
			result &= this.value2.equals(tuple.value2);
		}
		return result;
	}

	public String toString()
	{
		return "(" + this.value0.toString() + ", " + this.value1.toString() + ", " + this.value2.toString() + ")";
	}
}