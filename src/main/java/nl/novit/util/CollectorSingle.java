package nl.novit.util;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class CollectorSingle<Type>
	implements Collector<Type, List<Type>, Type>
{
	@Override
	public Supplier<List<Type>> supplier()
	{
		return LinkedList::new;
	}

	@Override
	public BiConsumer<List<Type>, Type> accumulator()
	{
		return List::add;
	}

	@Override
	public BinaryOperator<List<Type>> combiner()
	{
		return (list0, list1) ->
		{
			LinkedList<Type> result = new LinkedList<>();
			result.addAll(list0);
			result.addAll(list1);
			return result;
		};
	}

	@Override
	public Function<List<Type>, Type> finisher()
	{
		return list ->
		{
			if (list.size() == 1)
			{
				return list.get(0);
			}
			else
			{
				throw new NoSuchElementException();
			}
		};
	}

	@Override
	public Set<Characteristics> characteristics()
	{
		return Set.of(Characteristics.CONCURRENT, Characteristics.UNORDERED);
	}
}