package nl.novit.util;

public interface Function2Exception<TypeTarget, Type0, Type1>
{
	TypeTarget apply(Type0 value0, Type1 value1)
		throws Exception;
}