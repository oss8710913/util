package nl.novit.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class UtilCollections
{
	public static <Type extends Comparable<? super Type>> List<Type> listOf(Type value0)
	{
		List<Type> result = new ArrayList<>();
		result.add(value0);
		return result;
	}

	public static <Type extends Comparable<? super Type>> List<Type> listOf(Type value0, Type value1)
	{
		List<Type> result = new ArrayList<>();
		result.add(value0);
		result.add(value1);
		return result;
	}

	public static <Type extends Comparable<? super Type>> List<Type> listOf(Type value0, Type value1, Type value2)
	{
		List<Type> result = new ArrayList<>();
		result.add(value0);
		result.add(value1);
		result.add(value2);
		return result;
	}

	public static <Type extends Comparable<? super Type>> List<Type> listOf(Type value0, Type value1, Type value2, Type value3)
	{
		List<Type> result = new ArrayList<>();
		result.add(value0);
		result.add(value1);
		result.add(value2);
		result.add(value3);
		return result;
	}

	public static <Type extends Comparable<? super Type>> List<Type> listOf(Type value0, Type value1, Type value2, Type value3, Type value4)
	{
		List<Type> result = new ArrayList<>();
		result.add(value0);
		result.add(value1);
		result.add(value2);
		result.add(value3);
		result.add(value4);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> setOf(Type value0)
	{
		Set<Type> result = new TreeSet<>();
		result.add(value0);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> setOf(Type value0, Type value1)
	{
		Set<Type> result = new TreeSet<>();
		result.add(value0);
		result.add(value1);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> setOf(Type value0, Type value1, Type value2)
	{
		Set<Type> result = new TreeSet<>();
		result.add(value0);
		result.add(value1);
		result.add(value2);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> setOf(Type value0, Type value1, Type value2, Type value3)
	{
		Set<Type> result = new TreeSet<>();
		result.add(value0);
		result.add(value1);
		result.add(value2);
		result.add(value3);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> setOf(Type value0, Type value1, Type value2, Type value3, Type value4)
	{
		Set<Type> result = new TreeSet<>();
		result.add(value0);
		result.add(value1);
		result.add(value2);
		result.add(value3);
		result.add(value4);
		return result;
	}

	public static <TypeKey extends Comparable<? super TypeKey>, TypeValue> Map<TypeKey, TypeValue> mapOf(TypeKey key0, TypeValue value0)
	{
		Map<TypeKey, TypeValue> result = new TreeMap<>();
		result.put(key0, value0);
		return result;
	}

	public static <TypeKey extends Comparable<? super TypeKey>, TypeValue> Map<TypeKey, TypeValue> mapOf(TypeKey key0, TypeValue value0, TypeKey key1, TypeValue value1)
	{
		Map<TypeKey, TypeValue> result = new TreeMap<>();
		result.put(key0, value0);
		result.put(key1, value1);
		return result;
	}

	public static <TypeKey extends Comparable<? super TypeKey>, TypeValue> Map<TypeKey, TypeValue> mapOf(TypeKey key0, TypeValue value0, TypeKey key1, TypeValue value1, TypeKey key2, TypeValue value2)
	{
		Map<TypeKey, TypeValue> result = new TreeMap<>();
		result.put(key0, value0);
		result.put(key1, value1);
		result.put(key2, value2);
		return result;
	}

	public static <TypeKey extends Comparable<? super TypeKey>, TypeValue> Map<TypeKey, TypeValue> mapOf(TypeKey key0, TypeValue value0, TypeKey key1, TypeValue value1, TypeKey key2, TypeValue value2, TypeKey key3, TypeValue value3)
	{
		Map<TypeKey, TypeValue> result = new TreeMap<>();
		result.put(key0, value0);
		result.put(key1, value1);
		result.put(key2, value2);
		result.put(key3, value3);
		return result;
	}

	public static <TypeKey extends Comparable<? super TypeKey>, TypeValue> Map<TypeKey, TypeValue> mapOf(TypeKey key0, TypeValue value0, TypeKey key1, TypeValue value1, TypeKey key2, TypeValue value2, TypeKey key3, TypeValue value3, TypeKey key4, TypeValue value4)
	{
		Map<TypeKey, TypeValue> result = new TreeMap<>();
		result.put(key0, value0);
		result.put(key1, value1);
		result.put(key2, value2);
		result.put(key3, value3);
		result.put(key4, value4);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> union(Set<? extends Type> set0, Set<? extends Type> set1)
	{
		Set<Type> result = new TreeSet<>(set0);
		result.addAll(set1);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> union(Set<? extends Type> set0, Set<? extends Type> set1, Set<? extends Type> set2)
	{
		Set<Type> result = new TreeSet<>(set0);
		result.addAll(set1);
		result.addAll(set2);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> union(Set<? extends Type> set0, Set<? extends Type> set1, Set<? extends Type> set2, Set<? extends Type> set3)
	{
		Set<Type> result = new TreeSet<>(set0);
		result.addAll(set1);
		result.addAll(set2);
		result.addAll(set3);
		return result;
	}

	public static <Type extends Comparable<? super Type>> Set<Type> union(Set<? extends Type> set0, Set<? extends Type> set1, Set<? extends Type> set2, Set<? extends Type> set3, Set<? extends Type> set4)
	{
		Set<Type> result = new TreeSet<>(set0);
		result.addAll(set1);
		result.addAll(set2);
		result.addAll(set3);
		result.addAll(set4);
		return result;
	}
}