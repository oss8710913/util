package nl.novit.util;

public interface Function1<TypeTarget, Type0>
{
	TypeTarget apply(Type0 value0);
}