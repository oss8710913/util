package nl.novit.util;

public interface Function2<TypeTarget, Type0, Type1>
{
	TypeTarget apply(Type0 value0, Type1 value1);
}