package nl.novit.util.collections.linear.stack;

public class StackPopulated<Type>
	extends Stack<Type>
{
	public final Type value;
	public final Stack<Type> stack;

	public StackPopulated
		(
			Type value,
			Stack<Type> stack
		)
	{
		this.value = value;
		this.stack = stack;
	}

	@Override
	public boolean equalsVisit(StackEmpty<Type> that)
	{
		return false;
	}

	@Override
	public boolean equalsVisit(StackPopulated<Type> that)
	{
		return this.value.equals(that.value) && this.stack.equals(that.stack);
	}

	@Override
	public boolean equals(Stack<Type> that)
	{
		return that.equalsVisit(this);
	}

	@Override
	public boolean isEmpty()
	{
		return false;
	}

	@Override
	public Type peek()
	{
		return this.value;
	}

	@Override
	public Stack<Type> pop()
	{
		return this.stack;
	}
}