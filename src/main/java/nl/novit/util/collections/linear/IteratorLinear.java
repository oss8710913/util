package nl.novit.util.collections.linear;

import java.util.Iterator;

public class IteratorLinear<TypeLinear extends Linear<TypeLinear, Type>, Type>
	implements Iterator<Type>
{
	public TypeLinear linear;

	public IteratorLinear(TypeLinear linear)
	{
		this.linear = linear;
	}

	@Override
	public boolean hasNext()
	{
		return !this.linear.isEmpty();
	}

	@Override
	public Type next()
	{
		Type result = this.linear.peek();
		this.linear = this.linear.pop();
		return result;
	}
}