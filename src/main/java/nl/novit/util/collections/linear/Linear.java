package nl.novit.util.collections.linear;

import java.util.Iterator;

public abstract class Linear<TypeLinear extends Linear<TypeLinear, Type>, Type>
	implements Iterable<Type>
{
	public abstract TypeLinear getThis();

	public abstract boolean isEmpty();

	@Override
	public Iterator<Type> iterator()
	{
		return new IteratorLinear<>(getThis());
	}

	public abstract Type peek();

	public abstract TypeLinear pop();

	public abstract TypeLinear push(Type value);

	public TypeLinear push(Iterable<Type> values)
	{
		TypeLinear result = getThis();
		for (Type value: values)
		{
			result = result.push(value);
		}
		return result;
	}
}