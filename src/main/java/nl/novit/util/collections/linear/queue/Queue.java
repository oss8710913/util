package nl.novit.util.collections.linear.queue;

import nl.novit.util.Util;
import nl.novit.util.collections.linear.Linear;
import nl.novit.util.collections.linear.stack.Stack;
import nl.novit.util.collections.linear.stack.StackEmpty;

public class Queue<Type>
	extends Linear<Queue<Type>, Type>
{
	public final Stack<Type> head;
	public final Stack<Type> tail;

	public Queue
		(
			Stack<Type> head,
			Stack<Type> tail
		)
	{
		this.head = head;
		this.tail = tail;
	}

	public Queue()
	{
		this
			(
				new StackEmpty<>(),
				new StackEmpty<>()
			);
	}

	public boolean equals(Queue<Type> that)
	{
		return Util.equalsIterable(this, that);
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof Queue && equals((Queue<Type>) that);
	}

	@Override
	public Queue<Type> getThis()
	{
		return this;
	}

	public boolean isEmpty()
	{
		return this.head.isEmpty();
	}

	public Type peek()
	{
		return this.head.peek();
	}

	public Queue<Type> pop()
	{
		Stack<Type> head = this.head.pop();
		Queue<Type> result;
		if (head.isEmpty())
		{
			result =
				new Queue<>
					(
						new StackEmpty<Type>().push(this.tail),
						new StackEmpty<>()
					);
		}
		else
		{
			result =
				new Queue<>
					(
						head,
						this.tail
					);
		}
		return result;
	}

	public Queue<Type> push(Type value)
	{
		Queue<Type> result;
		if (this.head.isEmpty())
		{
			result =
				new Queue<>
					(
						this.head.push(value),
						this.tail
					);
		}
		else
		{
			result =
				new Queue<>
					(
						this.head,
						this.tail.push(value)
					);
		}
		return result;
	}
}