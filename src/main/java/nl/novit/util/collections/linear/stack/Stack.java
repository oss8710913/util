package nl.novit.util.collections.linear.stack;

import nl.novit.util.collections.linear.Linear;

public abstract class Stack<Type>
	extends Linear<Stack<Type>, Type>
{
	public abstract boolean equalsVisit(StackEmpty<Type> that);

	public abstract boolean equalsVisit(StackPopulated<Type> that);

	public abstract boolean equals(Stack<Type> that);

	@Override
	public boolean equals(Object that)
	{
		return that instanceof Stack && equals((Stack<Type>) that);
	}

	@Override
	public Stack<Type> getThis()
	{
		return this;
	}

	@Override
	public abstract boolean isEmpty();

	@Override
	public abstract Type peek();

	@Override
	public abstract Stack<Type> pop();

	@Override
	public Stack<Type> push(Type value)
	{
		return
			new StackPopulated<>
				(
					value,
					this
				);
	}
}