package nl.novit.util.collections.linear.stack;

import java.util.EmptyStackException;

public class StackEmpty<Type>
	extends Stack<Type>
{
	@Override
	public boolean equalsVisit(StackEmpty<Type> that)
	{
		return true;
	}

	@Override
	public boolean equalsVisit(StackPopulated<Type> that)
	{
		return false;
	}

	@Override
	public boolean equals(Stack<Type> that)
	{
		return that.equalsVisit(this);
	}

	@Override
	public boolean isEmpty()
	{
		return true;
	}

	@Override
	public Type peek()
	{
		throw new EmptyStackException();
	}

	@Override
	public Stack<Type> pop()
	{
		throw new EmptyStackException();
	}
}