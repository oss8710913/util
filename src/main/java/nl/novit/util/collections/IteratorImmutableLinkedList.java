package nl.novit.util.collections;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class IteratorImmutableLinkedList<Type>
	implements ListIterator<Type>
{
	public NodeImmutableLinkedList<Type> node;

	public IteratorImmutableLinkedList(NodeImmutableLinkedList<Type> node)
	{
		this.node = node;
	}

	@Override
	public boolean hasNext()
	{
		return this.node != null;
	}

	@Override
	public Type next()
	{
		if (this.node == null)
		{
			throw new NoSuchElementException();
		}
		Type result = this.node.element;
		this.node = this.node.tail;
		return result;
	}

	@Override
	public boolean hasPrevious()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Type previous()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int nextIndex()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int previousIndex()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void set(Type type)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(Type type)
	{
		throw new UnsupportedOperationException();
	}
}