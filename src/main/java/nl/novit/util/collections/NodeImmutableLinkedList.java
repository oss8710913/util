package nl.novit.util.collections;

public class NodeImmutableLinkedList<Type>
{
	public final Type element;
	public final NodeImmutableLinkedList<Type> tail;

	public NodeImmutableLinkedList(Type element, NodeImmutableLinkedList<Type> tail)
	{
		this.element = element;
		this.tail = tail;
	}

	public int size()
	{
		return (this.tail == null ? 0 : this.tail.size()) + 1;
	}

	public boolean contains(Object that)
	{
		return that.equals(this.element) || (this.tail != null && this.tail.contains(that));
	}
}