package nl.novit.util.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class ImmutableLinkedList<Type>
	implements List<Type>
{
	public final NodeImmutableLinkedList<Type> node;

	public static <T> NodeImmutableLinkedList<T> convert(Iterator<T> iterator, int size)
	{
		return 0 < size ? new NodeImmutableLinkedList<>(iterator.next(), convert(iterator, size - 1)) : null;
	}

	public ImmutableLinkedList(NodeImmutableLinkedList<Type> node)
	{
		this.node = node;
	}

	public ImmutableLinkedList()
	{
		this((NodeImmutableLinkedList<Type>) null);
	}

	public ImmutableLinkedList(Collection<Type> collection)
	{
		this((convert(collection.iterator(), collection.size())));
	}

	@Override
	public boolean isEmpty()
	{
		return this.node == null;
	}

	@Override
	public int size()
	{
		return isEmpty() ? 0 : this.node.size();
	}

	@Override
	public boolean contains(Object that)
	{
		return !isEmpty() && this.node.contains(that);
	}

	@Override
	public Iterator<Type> iterator()
	{
		return listIterator();
	}

	@Override
	public Object[] toArray()
	{
		Object[] result = new Object[size()];
		int index = 0;
		for (Type element: this)
		{
			result[index] = element;
			index += 1;
		}
		return result;
	}

	@Override
	public <T> T[] toArray(T[] ts)
	{
		T[] result = size() <= ts.length ? ts : Arrays.copyOf(ts, size());
		int index = 0;
		for (Type element: this)
		{
			result[index] = (T) element;
			index += 1;
		}
		while (index < ts.length)
		{
			ts[index] = null;
			index += 1;
		}
		return result;
	}

	@Override
	public boolean add(Type type)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> collection)
	{
		boolean result = true;
		for (Object element: collection)
		{
			result &= contains(element);
		}
		return result;
	}

	@Override
	public boolean addAll(Collection<? extends Type> collection)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> collection)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> collection)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int i, Collection<? extends Type> collection)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Type get(int index)
	{
		try
		{
			Iterator<Type> iterator = listIterator(index);
			return iterator.next();
		}
		catch (
			NoSuchElementException exception)
		{
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public Type set(int i, Type type)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int i, Type type)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Type remove(int i)
	{
		throw new UnsupportedOperationException();
	}

	public int indexOf(Object o, boolean last)
	{
		int result = -1;
		Iterator<Type> iterator = iterator();
		int index = 0;
		while ((last || result == -1) && iterator.hasNext())
		{
			if (iterator.next().equals(o))
			{
				result = index;
			}
			index += 1;
		}
		return result;
	}

	@Override
	public int indexOf(Object o)
	{
		return indexOf(o, false);
	}

	@Override
	public int lastIndexOf(Object o)
	{
		return indexOf(o, true);
	}

	@Override
	public ListIterator<Type> listIterator(int index)
	{
		if (index < 0)
		{
			throw new IndexOutOfBoundsException();
		}
		ListIterator<Type> result = new IteratorImmutableLinkedList<>(this.node);
		while (0 < index)
		{
			try
			{
				result.next();
			}
			catch (
				NoSuchElementException exception)
			{
				throw new IndexOutOfBoundsException();
			}
			index -= 1;
		}
		return result;
	}

	@Override
	public ListIterator<Type> listIterator()
	{
		return listIterator(0);
	}

	@Override
	public ImmutableLinkedList<Type> subList(int start, int end)
	{
		if (end < start)
		{
			throw new IndexOutOfBoundsException();
		}
		try
		{
			Iterator<Type> iterator = listIterator(start);
			return new ImmutableLinkedList<>(convert(iterator, end - start));
		}
		catch (
			NoSuchElementException exception)
		{
			throw new IndexOutOfBoundsException();
		}
	}

	public boolean equals(ImmutableLinkedList<Type> that)
	{
		boolean result = true;
		Iterator<Type> iteratorThis = iterator();
		Iterator<Type> iteratorThat = that.iterator();
		while (iteratorThis.hasNext() && iteratorThat.hasNext())
		{
			result &= iteratorThis.next().equals(iteratorThat.next());
		}
		result &= iteratorThis.hasNext() == iteratorThat.hasNext();
		return result;
	}

	public boolean equals(Object that)
	{
		return that instanceof ImmutableLinkedList && equals((ImmutableLinkedList<Type>) that);
	}

	public Type first()
	{
		if (this.node == null)
		{
			throw new IndexOutOfBoundsException();
		}
		return this.node.element;
	}

	public ImmutableLinkedList<Type> addFirst(Type element)
	{
		return new ImmutableLinkedList<>
			(
				new NodeImmutableLinkedList<>
					(
						element,
						this.node
					)
			);
	}

	public ImmutableLinkedList<Type> removeFirst()
	{
		if (this.node == null)
		{
			throw new IndexOutOfBoundsException();
		}
		return new ImmutableLinkedList<>(this.node.tail);
	}
}