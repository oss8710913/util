package nl.novit.util.collections.set.set23;

import nl.novit.util.Wrapper;
import nl.novit.util.collections.linear.stack.Stack;
import nl.novit.util.collections.linear.stack.StackEmpty;

import java.util.Iterator;

public class IteratorSet<Type>
	implements Iterator<Type>
{
	public final Wrapper<Stack<Node<Type>>> stack;

	public IteratorSet(Node<Type> root)
	{
		this.stack = new Wrapper<>(new StackEmpty<>());
		if (root != null)
		{
			this.stack.value = this.stack.value.push(root);
		}
	}

	@Override
	public boolean hasNext()
	{
		return !this.stack.value.isEmpty();
	}

	@Override
	public Type next()
	{
		return stack.value.peek().next(this.stack);
	}
}