package nl.novit.util.collections.set;

import java.util.Comparator;
import java.util.Optional;

public interface Node<TypeNode extends Node<TypeNode, Type>, Type>
{
	static <TypeNode extends Node<TypeNode, Type>, Type> Optional<? extends NodeValue<TypeNode, Type>> get
		(
			Comparator<? super Type> comparator,
			TypeNode node,
			Type value
		)
	{
		return node == null ?
			Optional.empty() :
			node.get
				(
					comparator,
					value
				);
	}

	static <TypeNode extends Node<TypeNode, Type>, Type> long size(TypeNode node)
	{
		return node == null ? 0 : node.size();
	}

	NodeValue<TypeNode, Type> first();

	NodeValue<TypeNode, Type> last();

	Optional<? extends NodeValue<TypeNode, Type>> get
		(
			Comparator<? super Type> comparator,
			Type value
		);

	long size();
}