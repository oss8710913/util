package nl.novit.util.collections.set;

import nl.novit.util.Util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;

public interface MapDefault<TypeNode extends Node<TypeNode, Tuple2Map<TypeKey, TypeValue>>, TypeKey, TypeValue>
	extends Map<TypeKey, TypeValue>
{
	static <TypeKey, TypeValue> Tuple2Map<TypeKey, TypeValue> tuple(TypeKey key)
	{
		return new Tuple2Map<>(key, null);
	}

	static <TypeKey, TypeValue> Comparator<? super Tuple2Map<TypeKey, TypeValue>> comparator(final Comparator<? super TypeKey> comparator)
	{
		return (tuple0, tuple1) -> comparator.compare(tuple0.value0, tuple1.value0);
	}

	SetDefault<TypeNode, Tuple2Map<TypeKey, TypeValue>> add(Tuple2Map<TypeKey, TypeValue> value);

	@Override
	default Map<TypeKey, TypeValue> addKey(TypeKey key, TypeValue value)
	{
		Tuple2Map<TypeKey, TypeValue> tuple = new Tuple2Map<>(key, value);
		return map(remove(tuple).add(tuple));
	}

	Comparator<? super Tuple2Map<TypeKey, TypeValue>> comparator();

	@Override
	default int compareTo(Set<Tuple2Map<TypeKey, TypeValue>> that)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	default boolean containsKey(TypeKey key)
	{
		return getKey(key).isPresent();
	}

	@Override
	default boolean equals(Map<TypeKey, TypeValue> that)
	{
		return
			Util.compareIterable
				(
					(tuple0, tuple1) ->
					{
						int result = comparator().compare(tuple0, tuple1);
						if (result == 0)
						{
							result = tuple0.equals(tuple1) ? 0 : 1;
						}
						return result;
					},
					this,
					that
				) == 0;
	}

	Optional<? extends NodeValue<TypeNode, Tuple2Map<TypeKey, TypeValue>>> get(Tuple2Map<TypeKey, TypeValue> value);

	@Override
	default Optional<TypeValue> getKey(TypeKey key)
	{
		return get(tuple(key)).map(x -> x.value().value1);
	}

	@Override
	default Map<TypeKey, TypeValue> headMap(TypeKey typeKey)
	{
		return map(headSet(tuple(typeKey)));
	}

	SetDefault<TypeNode, Tuple2Map<TypeKey, TypeValue>> headSet(Tuple2Map<TypeKey, TypeValue> value);

	@Override
	default Iterable<TypeKey> keys()
	{
		final Iterator<Tuple2Map<TypeKey, TypeValue>> iterator = iterator();
		return () -> new Iterator<>()
		{
			@Override
			public boolean hasNext()
			{
				return iterator.hasNext();
			}

			@Override
			public TypeKey next()
			{
				return iterator.next().key();
			}
		};
	}

	Map<TypeKey, TypeValue> map(SetDefault<TypeNode, Tuple2Map<TypeKey, TypeValue>> set);

	SetDefault<TypeNode, Tuple2Map<TypeKey, TypeValue>> remove(Tuple2Map<TypeKey, TypeValue> value);

	@Override
	default Map<TypeKey, TypeValue> removeKey(TypeKey key)
	{
		return map(remove(tuple(key)));
	}

	@Override
	default Map<TypeKey, TypeValue> tailMap(TypeKey key)
	{
		return map(tailSet(tuple(key)));
	}

	SetDefault<TypeNode, Tuple2Map<TypeKey, TypeValue>> tailSet(Tuple2Map<TypeKey, TypeValue> value);

	@Override
	default Iterable<TypeValue> values()
	{
		final Iterator<Tuple2Map<TypeKey, TypeValue>> iterator = iterator();
		return () -> new Iterator<>()
		{
			@Override
			public boolean hasNext()
			{
				return iterator.hasNext();
			}

			@Override
			public TypeValue next()
			{
				return iterator.next().value();
			}
		};
	}
}