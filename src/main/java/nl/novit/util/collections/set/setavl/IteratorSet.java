package nl.novit.util.collections.set.setavl;

import nl.novit.util.collections.linear.stack.Stack;
import nl.novit.util.collections.linear.stack.StackEmpty;

import java.util.Iterator;

public class IteratorSet<Type>
	implements Iterator<Type>
{
	public Stack<Node<Type>> stack;

	public void left(Node<Type> node)
	{
		if (node != null)
		{
			this.stack = this.stack.push(node);
			left(node.left);
		}
	}

	public IteratorSet(Node<Type> node)
	{
		this.stack = new StackEmpty<>();
		left(node);
	}

	@Override
	public boolean hasNext()
	{
		return !this.stack.isEmpty();
	}

	@Override
	public Type next()
	{
		Node<Type> result = this.stack.peek();
		if (result.right == null)
		{
			Node<Type> node;
			do
			{
				node = this.stack.peek();
				this.stack = this.stack.pop();
			}
			while (!this.stack.isEmpty() && this.stack.peek().right == node);
		}
		else
		{
			left(result.right);
		}
		return result.value;
	}
}