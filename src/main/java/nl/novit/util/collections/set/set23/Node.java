package nl.novit.util.collections.set.set23;

import nl.novit.util.Function1;
import nl.novit.util.Wrapper;
import nl.novit.util.collections.linear.stack.Stack;

import java.util.Comparator;

public interface Node<Type>
	extends nl.novit.util.collections.set.Node<Node<Type>, Type>
{
	static <Type> Node<Type> head
		(
			final Comparator<? super Type> comparator,
			final Node<Type> root,
			final Type value
		)
	{
		return Node.split(x -> comparator.compare(x, value) < 0, root).node0;
	}

	static <Type> Node<Type> tailExclusive
		(
			final Comparator<? super Type> comparator,
			final Node<Type> root,
			final Type value
		)
	{
		return Node.split(x -> comparator.compare(x, value) <= 0, root).node1;
	}

	static <Type> Node<Type> merge
		(
			Node<Type> node0,
			Node<Type> node1
		)
	{
		Node<Type> result;
		if (node0 == null)
		{
			if (node1 == null)
			{
				result = null;
			}
			else
			{
				result = node1;
			}
		}
		else
		{
			if (node1 == null)
			{
				result = node0;
			}
			else
			{
				result = node0.merge(node1).node();
			}
		}
		return result;
	}

	static <Type> Nodes2<Type> split
		(
			Function1<Boolean, Type> function,
			Node<Type> node
		)
	{
		return node == null ? new Nodes2<>(null, null) : node.split(function);
	}

	int height();

	Nodes<Type> mergeVisit(NodeLeaf<Type> that);

	Nodes<Type> mergeVisit(NodeInternal<Type> that);

	Nodes<Type> merge(Node<Type> that);

	Type next(Wrapper<Stack<Node<Type>>> stack);

	Type smallest();

	Nodes2<Type> split(Function1<Boolean, Type> function);
}