package nl.novit.util.collections.set;

public interface NodeValue<TypeNode extends Node<? extends TypeNode, Type>, Type>
{
	Type value();

	TypeNode getThis();
}