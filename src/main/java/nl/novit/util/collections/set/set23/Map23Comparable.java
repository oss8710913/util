package nl.novit.util.collections.set.set23;

import java.util.Comparator;

public class Map23Comparable<TypeKey extends Comparable<? super TypeKey>, TypeValue>
	extends Map23<TypeKey, TypeValue>
{
	public Map23Comparable()
	{
		super(Comparator.naturalOrder());
	}
}