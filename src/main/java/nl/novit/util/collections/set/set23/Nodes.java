package nl.novit.util.collections.set.set23;

public abstract class Nodes<Type>
{
	public abstract Nodes<Type> mergeVisit(Nodes1<Type> that);

	public abstract Nodes<Type> mergeVisit(Nodes2<Type> that);

	public abstract Nodes<Type> merge(Nodes<Type> that);

	public abstract Node<Type> node();
}