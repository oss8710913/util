package nl.novit.util.collections.set.setavl;

import nl.novit.util.collections.set.Map;
import nl.novit.util.collections.set.MapDefault;
import nl.novit.util.collections.set.SetDefault;
import nl.novit.util.collections.set.Tuple2Map;

import java.util.Comparator;

public class MapAVL<TypeKey, TypeValue>
	extends SetAVL<Tuple2Map<TypeKey, TypeValue>>
	implements MapDefault<Node<Tuple2Map<TypeKey, TypeValue>>, TypeKey, TypeValue>
{
	public MapAVL
		(
			Comparator<? super Tuple2Map<TypeKey, TypeValue>> comparator,
			Node<Tuple2Map<TypeKey, TypeValue>> root
		)
	{
		super
			(
				comparator,
				root
			);
	}

	public MapAVL
		(
			Comparator<? super TypeKey> comparator
		)
	{
		this
			(
				MapDefault.comparator(comparator),
				null
			);
	}

	@Override
	public Comparator<? super Tuple2Map<TypeKey, TypeValue>> comparator()
	{
		return this.comparator;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof Map && equals((Map<TypeKey, TypeValue>) that);
	}

	@Override
	public Map<TypeKey, TypeValue> map(SetDefault<Node<Tuple2Map<TypeKey, TypeValue>>, Tuple2Map<TypeKey, TypeValue>> set)
	{
		return
			new MapAVL<>
				(
					set.comparator,
					set.root
				);
	}
}