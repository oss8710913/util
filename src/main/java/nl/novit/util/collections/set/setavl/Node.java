package nl.novit.util.collections.set.setavl;

import nl.novit.util.collections.set.NodeValue;

import java.util.Comparator;
import java.util.Optional;

public class Node<Type>
	implements nl.novit.util.collections.set.Node<Node<Type>, Type>, NodeValue<Node<Type>, Type>
{
	public static <Type> Node<Type> add(Comparator<? super Type> comparator, Node<Type> node, Type value)
	{
		Node<Type> result;
		if (node == null)
		{
			result = new Node<>(value, null, null);
		}
		else
		{
			int compare = comparator.compare(value, node.value);
			if (compare == 0)
			{
				result = node;
			}
			else
			{
				if (compare < 0)
				{
					result = node.left(add(comparator, node.left, value));
				}
				else
				{
					result = node.right(add(comparator, node.right, value));
				}
				result = Node.balance(result);
			}
		}
		return result;
	}

	public static <Type> Node<Type> balance(Node<Type> result)
	{
		if (result != null)
		{
			if (1 < height(result.left) - height(result.right))
			{
				Node<Type> left = result.left;
				if (height(left.left) < height(left.right))
				{
					result = new Node<>(result.value, left.rotateLeft(), result.right);
				}
				result = result.rotateRight();
			}
			if (1 < height(result.right) - height(result.left))
			{
				Node<Type> right = result.right;
				if (height(right.right) < height(right.left))
				{
					result = new Node<>(result.value, result.left, right.rotateRight());
				}
				result = result.rotateLeft();
			}
		}
		return result;
	}

	public static <Type> Node<Type> headSet(Comparator<? super Type> comparator, Node<Type> node, Type value)
	{
		Node<Type> result;
		if (node == null)
		{
			result = null;
		}
		else
		{
			int compare = comparator.compare(value, node.value);
			if (compare == 0)
			{
				result = node.left;
			}
			else
			{
				if (compare < 0)
				{
					result = headSet(comparator, node.left, value);
				}
				else
				{
					result = node.right(headSet(comparator, node.right, value));
				}
				result = Node.balance(result);
			}
		}
		return result;
	}

	public static int height(Node<?> node)
	{
		return node == null ? 0 : node.height;
	}

	public static <Type> Node<Type> remove(Comparator<? super Type> comparator, Node<Type> node, Type value)
	{
		Node<Type> result;
		if (node == null)
		{
			result = null;
		}
		else
		{
			int compare = comparator.compare(value, node.value);
			if (compare == 0)
			{
				if (node.left == null)
				{
					result = node.right;
				}
				else
				{
					if (node.right == null)
					{
						result = node.left;
					}
					else
					{
						Node<Type> first = node.right.first();
						result = new Node<>(first.value, node.left, remove(comparator, node.right, first.value));
					}
				}
			}
			else
			{
				if (compare < 0)
				{
					result = node.left(remove(comparator, node.left, value));
				}
				else
				{
					result = node.right(remove(comparator, node.right, value));
				}
			}
			result = Node.balance(result);
		}
		return result;
	}

	public static <Type> Node<Type> tailSet(Comparator<? super Type> comparator, Node<Type> node, Type value)
	{
		Node<Type> result;
		if (node == null)
		{
			result = null;
		}
		else
		{
			int compare = comparator.compare(value, node.value);
			if (compare == 0)
			{
				result = node.left(null);
			}
			else
			{
				if (compare < 0)
				{
					result = node.left(tailSet(comparator, node.left, value));
				}
				else
				{
					result = tailSet(comparator, node.right, value);
				}
			}
			result = Node.balance(result);
		}
		return result;
	}

	public static <Type> Type value(Node<Type> node)
	{
		return node == null ? null : node.value;
	}

	public final Type value;
	public final Node<Type> left;
	public final Node<Type> right;
	public final byte height;

	public Node(Type value, Node<Type> left, Node<Type> right)
	{
		this.value = value;
		this.left = left;
		this.right = right;
		this.height = (byte) (1 + Math.max(height(left), height(right)));
	}

	@Override
	public Node<Type> first()
	{
		return this.left == null ? this : this.left.first();
	}

	public Optional<? extends NodeValue<Node<Type>, Type>> get(Comparator<? super Type> comparator, Type value)
	{
		Optional<? extends NodeValue<Node<Type>, Type>> result;
		int compare = comparator.compare(value, this.value);
		if (compare == 0)
		{
			result = Optional.of(this);
		}
		else
		{
			if (compare < 0)
			{
				result = nl.novit.util.collections.set.Node.get(comparator, this.left, value);
			}
			else
			{
				result = nl.novit.util.collections.set.Node.get(comparator, this.right, value);
			}
		}
		return result;
	}

	@Override
	public Node getThis()
	{
		return null;
	}

	@Override
	public Node<Type> last()
	{
		return this.right == null ? this : this.right.last();
	}

	public Node<Type> left(Node<Type> left)
	{
		return this.left == left ? this : new Node<>(this.value, left, this.right);
	}

	public Node<Type> right(Node<Type> right)
	{
		return this.right == right ? this : new Node<>(this.value, this.left, right);
	}

	public Node<Type> rotateLeft()
	{
		Node<Type> left = new Node<>(this.value, this.left, this.right.left);
		return new Node<>(this.right.value, left, this.right.right);
	}

	public Node<Type> rotateRight()
	{
		Node<Type> right = new Node<>(this.value, this.left.right, this.right);
		return new Node<>(this.left.value, this.left.left, right);
	}

	public long size()
	{
		return
			1 +
				nl.novit.util.collections.set.Node.size(this.left) +
				nl.novit.util.collections.set.Node.size(this.right);
	}

	@Override
	public Type value()
	{
		return this.value;
	}
}