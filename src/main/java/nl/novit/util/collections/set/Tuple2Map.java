package nl.novit.util.collections.set;

import nl.novit.util.Tuple2;

public class Tuple2Map<TypeKey, TypeValue>
	extends Tuple2<TypeKey, TypeValue>
{
	public Tuple2Map(TypeKey value0, TypeValue value1)
	{
		super(value0, value1);
	}

	public TypeKey key()
	{
		return this.value0;
	}

	public TypeValue value()
	{
		return this.value1;
	}

	@Override
	public String toString()
	{
		return "(" + key() + " -> " + value() + ')';
	}
}