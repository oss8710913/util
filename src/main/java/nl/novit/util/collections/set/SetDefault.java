package nl.novit.util.collections.set;

import nl.novit.util.Util;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class SetDefault<TypeNode extends Node<TypeNode, Type>, Type>
	implements Set<Type>
{
	public final Comparator<? super Type> comparator;
	public final TypeNode root;

	public SetDefault
		(
			Comparator<? super Type> comparator,
			TypeNode root
		)
	{
		this.comparator = comparator;
		this.root = root;
	}

	public abstract SetDefault<TypeNode, Type> add(Type value);

	@Override
	public SetDefault<TypeNode, Type> add(Iterable<? extends Type> values)
	{
		SetDefault<TypeNode, Type> result = this;
		for (Type value: values)
		{
			result = result.add(value);
		}
		return result;
	}

	@Override
	public int compareTo(Set<Type> that)
	{
		return Util.compareIterable(this.comparator, this, that);
	}

	@Override
	public boolean contains(Type value)
	{
		return get(value).isPresent();
	}

	@Override
	public boolean contains(Iterable<? extends Type> values)
	{
		boolean result = true;
		for (Type value: values)
		{
			result &= contains(value);
		}
		return result;
	}

	@Override
	public boolean equals(Set<Type> that)
	{
		return compareTo(that) == 0;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof Set && equals((Set<Type>) that); // TODO
	}

	@Override
	public Optional<Type> first()
	{
		return this.root == null ? Optional.empty() : Optional.of(this.root.first().value());
	}

	public Optional<? extends NodeValue<TypeNode, Type>> get(Type value)
	{
		return Node.get(this.comparator, this.root, value);
	}

	@Override
	public abstract SetDefault<TypeNode, Type> headSet(Type value);

	@Override
	public boolean isEmpty()
	{
		return this.root == null;
	}

	@Override
	public Optional<Type> last()
	{
		return this.root == null ? Optional.empty() : Optional.of(this.root.last().value());
	}

	@Override
	public abstract SetDefault<TypeNode, Type> remove(Type value);

	@Override
	public SetDefault<TypeNode, Type> remove(Iterable<? extends Type> values)
	{
		SetDefault<TypeNode, Type> result = this;
		for (Type value: values)
		{
			result = result.remove(value);
		}
		return result;
	}

	@Override
	public long size()
	{
		return Node.size(this.root);
	}

	public Stream<Type> stream()
	{
		return StreamSupport.stream(spliterator(), false); // TODO parallel?
	}

	@Override
	public abstract SetDefault<TypeNode, Type> tailSet(Type value);

	@Override
	public String toString()
	{
		return Util.toString(this);
	}
}