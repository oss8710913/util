package nl.novit.util.collections.set.setavl;

import nl.novit.util.collections.set.SetDefault;

import java.util.Comparator;
import java.util.Iterator;

public class SetAVL<Type>
	extends SetDefault<Node<Type>, Type>
{
	public SetAVL
		(
			Comparator<? super Type> comparator,
			Node<Type> root
		)
	{
		super
			(
				comparator,
				root
			);
	}

	public SetAVL(Comparator<Type> comparator)
	{
		this
			(
				comparator,
				null
			);
	}

	@Override
	public SetAVL<Type> add(Type value)
	{
		return set(Node.add(this.comparator, this.root, value));
	}

	@Override
	public SetAVL<Type> headSet(Type value)
	{
		return set(Node.headSet(this.comparator, this.root, value));
	}

	@Override
	public Iterator<Type> iterator()
	{
		return new IteratorSet<>(this.root);
	}

	@Override
	public SetAVL<Type> remove(Type value)
	{
		return set(Node.remove(this.comparator, this.root, value));
	}

	public SetAVL<Type> set(Node<Type> node)
	{
		return this.root == node ? this : new SetAVL<>(this.comparator, node);
	}

	@Override
	public SetAVL<Type> tailSet(Type value)
	{
		return set(Node.tailSet(this.comparator, this.root, value));
	}
}