package nl.novit.util.collections.set;

import java.util.Optional;

public interface Map<TypeKey, TypeValue>
	extends Set<Tuple2Map<TypeKey, TypeValue>>
{
	Map<TypeKey, TypeValue> addKey(TypeKey key, TypeValue value);

	boolean containsKey(TypeKey key);

	boolean equals(Map<TypeKey, TypeValue> that);

	Optional<TypeValue> getKey(TypeKey key);

	Map<TypeKey, TypeValue> headMap(TypeKey key);

	Iterable<TypeKey> keys();

	Map<TypeKey, TypeValue> removeKey(TypeKey key);

	Map<TypeKey, TypeValue> tailMap(TypeKey key);

	Iterable<TypeValue> values();
}