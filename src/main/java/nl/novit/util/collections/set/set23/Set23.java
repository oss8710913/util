package nl.novit.util.collections.set.set23;

import nl.novit.util.collections.set.SetDefault;

import java.util.Comparator;
import java.util.Iterator;

public class Set23<Type>
	extends SetDefault<Node<Type>, Type>
{
	public Set23
		(
			Comparator<? super Type> comparator,
			Node<Type> root
		)
	{
		super
			(
				comparator,
				root
			);
	}

	public Set23
		(
			Comparator<? super Type> comparator
		)
	{
		this
			(
				comparator,
				null
			);
	}

	@Override
	public SetDefault<Node<Type>, Type> add(Type value)
	{
		return
			set
				(
					Node.merge
						(
							Node.head
								(
									this.comparator,
									this.root,
									value
								),
							Node.merge
								(
									new NodeLeaf<>(value),
									Node.tailExclusive
										(
											this.comparator,
											this.root,
											value
										)
								)
						)
				);
	}

	public Set23<Type> headSet
		(
			Type value
		)
	{
		return
			new Set23<>
				(
					this.comparator,
					Node.split(x -> this.comparator.compare(x, value) < 0, this.root).node0
				);
	}

	@Override
	public Iterator<Type> iterator()
	{
		return new IteratorSet<>(this.root);
	}

	public Set23<Type> merge
		(
			Set23<Type> that
		)
	{
		return
			new Set23<>
				(
					this.comparator,
					Node.merge
						(
							this.root,
							that.root
						)
				);
	}

	@Override
	public SetDefault<Node<Type>, Type> remove(Type value)
	{
		return
			set
				(
					Node.merge
						(
							Node.head
								(
									this.comparator,
									this.root,
									value
								),
							Node.tailExclusive
								(
									this.comparator,
									this.root,
									value
								)
						)
				);
	}

	public Set23<Type> set(Node<Type> root)
	{
		return new Set23<>(this.comparator, root);
	}

	public Set23<Type> tailSet
		(
			Type value
		)
	{
		return
			new Set23<>
				(
					this.comparator,
					Node.split(x -> this.comparator.compare(x, value) < 0, this.root).node1
				);
	}
}