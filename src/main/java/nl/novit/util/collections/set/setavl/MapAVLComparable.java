package nl.novit.util.collections.set.setavl;

import java.util.Comparator;

public class MapAVLComparable<TypeKey extends Comparable<? super TypeKey>, TypeValue>
	extends MapAVL<TypeKey, TypeValue>
{
	public MapAVLComparable()
	{
		super(Comparator.naturalOrder());
	}
}