package nl.novit.util.collections.set.set23;

import nl.novit.util.Function1;
import nl.novit.util.Wrapper;
import nl.novit.util.collections.linear.stack.Stack;
import nl.novit.util.collections.set.NodeValue;

import java.util.Comparator;
import java.util.Optional;

public class NodeLeaf<Type>
	implements Node<Type>, NodeValue<Node<Type>, Type>
{
	public final Type value;

	public NodeLeaf
		(
			Type value
		)
	{
		this.value = value;
	}

	@Override
	public Optional<NodeValue<Node<Type>, Type>> get(Comparator<? super Type> comparator, Type value)
	{
		return comparator.compare(value, this.value) == 0 ? Optional.of(this) : Optional.empty();
	}

	@Override
	public NodeLeaf<Type> getThis()
	{
		return this;
	}

	@Override
	public NodeLeaf<Type> first()
	{
		return this;
	}

	@Override
	public int height()
	{
		return 0;
	}

	@Override
	public NodeLeaf<Type> last()
	{
		return this;
	}

	@Override
	public Nodes<Type> mergeVisit(NodeLeaf<Type> that)
	{
		return
			new Nodes2<>
				(
					that,
					this
				);
	}

	@Override
	public Nodes<Type> mergeVisit(NodeInternal<Type> that)
	{
		return that.firstOnes().merge(that.lastOne().merge(this));
	}

	@Override
	public Nodes<Type> merge(Node<Type> that)
	{
		return that.mergeVisit(this);
	}

	public Type next(Wrapper<Stack<Node<Type>>> stack)
	{
		stack.value = stack.value.pop();
		return this.value;
	}

	@Override
	public long size()
	{
		return 1;
	}

	@Override
	public Type smallest()
	{
		return this.value;
	}

	@Override
	public Nodes2<Type> split(Function1<Boolean, Type> function)
	{
		return
			function.apply(this.value) ?
				new Nodes2<>
					(
						this,
						null
					) :
				new Nodes2<>
					(
						null,
						this
					);
	}

	@Override
	public Type value()
	{
		return this.value;
	}
}