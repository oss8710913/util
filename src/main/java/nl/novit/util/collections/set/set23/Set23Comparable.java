package nl.novit.util.collections.set.set23;

import java.util.Comparator;

public class Set23Comparable<Type extends Comparable<? super Type>>
	extends Set23<Type>
{
	public Set23Comparable()
	{
		super
			(
				Comparator.naturalOrder()
			);
	}
}