package nl.novit.util.collections.set.setavl;

import java.util.Comparator;

public class SetAVLComparable<Type extends Comparable<? super Type>>
	extends SetAVL<Type>
{
	public SetAVLComparable()
	{
		super(Comparator.naturalOrder());
	}
}