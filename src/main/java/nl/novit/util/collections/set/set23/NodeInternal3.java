package nl.novit.util.collections.set.set23;

import nl.novit.util.Function1;
import nl.novit.util.Wrapper;
import nl.novit.util.collections.linear.stack.Stack;
import nl.novit.util.collections.set.NodeValue;

import java.util.Comparator;
import java.util.Optional;

public class NodeInternal3<Type>
	extends NodeInternal<Type>
{
	public final Node<Type> node0;
	public final Node<Type> node1;
	public final Node<Type> node2;

	public NodeInternal3(Type smallest, int height, Node<Type> node0, Node<Type> node1, Node<Type> node2)
	{
		super(smallest, height);
		this.node0 = node0;
		this.node1 = node1;
		this.node2 = node2;
	}

	public NodeInternal3
		(
			Node<Type> node0,
			Node<Type> node1,
			Node<Type> node2
		)
	{
		this
			(
				node0.smallest(),
				Math.max
					(
						Math.max
							(
								node0.height(),
								node1.height()
							),
						node2.height()
					),
				node0,
				node1,
				node2
			);
	}

	@Override
	public Node<Type> firstOne()
	{
		return this.node0;
	}

	@Override
	public Nodes<Type> firstOnes()
	{
		return
			new Nodes2<>
				(
					this.node0,
					this.node1
				);
	}

	@Override
	public Optional<? extends NodeValue<Node<Type>, Type>> get(Comparator<? super Type> comparator, Type value)
	{
		Optional<? extends NodeValue<Node<Type>, Type>> result;
		;
		if (comparator.compare(value, this.node1.smallest()) < 0)
		{
			result = this.node0.get(comparator, value);
		}
		else
		{
			if (comparator.compare(value, this.node2.smallest()) < 0)
			{
				result = this.node1.get(comparator, value);
			}
			else
			{
				result = this.node2.get(comparator, value);
			}
		}
		return result;
	}

	@Override
	public int height()
	{
		return this.height;
	}

	@Override
	public Node<Type> lastOne()
	{
		return this.node2;
	}

	@Override
	public Nodes<Type> lastOnes()
	{
		return
			new Nodes2<>
				(
					this.node1,
					this.node2
				);
	}

	@Override
	public Type next(Wrapper<Stack<Node<Type>>> stack)
	{
		stack.value = stack.value.pop();
		stack.value = stack.value.push(this.node2);
		stack.value = stack.value.push(this.node1);
		stack.value = stack.value.push(this.node0);
		return stack.value.peek().next(stack);
	}

	@Override
	public long size()
	{
		return this.node0.size() + this.node1.size() + this.node2.size();
	}

	@Override
	public Type smallest()
	{
		return this.smallest;
	}

	@Override
	public Nodes2<Type> split
		(
			Function1<Boolean, Type> function
		)
	{
		Nodes2<Type> result;
		if (function.apply(this.node1.smallest()))
		{
			if (function.apply(this.node2.smallest()))
			{
				Nodes2<Type> split =
					this.node2.split(function);
				result =
					new Nodes2<>
						(
							Node.merge(new NodeInternal2<>(this.node0, this.node1), split.node0),
							split.node1
						);
			}
			else
			{
				Nodes2<Type> split =
					this.node1.split(function);
				result =
					new Nodes2<>
						(
							Node.merge(this.node0, split.node0),
							Node.merge(split.node1, this.node2)
						);
			}
		}
		else
		{
			Nodes2<Type> split =
				this.node0.split(function);
			result =
				new Nodes2<>
					(
						split.node0,
						Node.merge(split.node1, new NodeInternal2<>(this.node1, this.node2))
					);
		}
		return result;
	}
}