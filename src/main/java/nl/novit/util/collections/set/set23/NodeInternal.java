package nl.novit.util.collections.set.set23;

import nl.novit.util.collections.set.NodeValue;

import java.util.Comparator;

public abstract class NodeInternal<Type>
	implements Node<Type>
{
	public static <Type> Type smallest
		(
			Comparator<Type> comparator,
			Type value0,
			Type value1
		)
	{
		return comparator.compare(value0, value1) < 0 ? value0 : value1;
	}

	public final Type smallest;
	public final int height;

	public NodeInternal
		(
			Type smallest,
			int height
		)
	{
		this.smallest = smallest;
		this.height = 1 + height;
	}

	@Override
	public NodeValue<Node<Type>, Type> first()
	{
		return firstOne().first();
	}

	public abstract Node<Type> firstOne();

	public abstract Nodes<Type> firstOnes();

	@Override
	public NodeValue<Node<Type>, Type> last()
	{
		return lastOne().last();
	}

	public abstract Node<Type> lastOne();

	public abstract Nodes<Type> lastOnes();

	@Override
	public Nodes<Type> mergeVisit(NodeLeaf<Type> that)
	{
		return that.merge(this.firstOne()).merge(this.lastOnes());
	}

	@Override
	public Nodes<Type> mergeVisit(NodeInternal<Type> that)
	{
		Nodes<Type> result;
		if (height() == that.height)
		{
			result =
				new Nodes2<>
					(
						that,
						this
					);
		}
		else
		{
			if (height() < that.height)
			{
				result = that.firstOnes().merge(that.lastOne().merge(this));
			}
			else
			{
				result = that.merge(firstOne()).merge(lastOnes());
			}
		}
		return result;
	}

	@Override
	public Nodes<Type> merge(Node<Type> that)
	{
		return that.mergeVisit(this);
	}
}