package nl.novit.util.collections.set.set23;

public class Nodes1<Type>
	extends Nodes<Type>
{
	public final Node<Type> node0;

	public Nodes1
		(
			Node<Type> node0
		)
	{
		this.node0 = node0;
	}

	@Override
	public Node<Type> node()
	{
		return this.node0;
	}

	@Override
	public Nodes<Type> mergeVisit(Nodes1<Type> that)
	{
		return
			new Nodes1<>
				(
					new NodeInternal2<>
						(
							that.node0,
							this.node0
						)
				);
	}

	@Override
	public Nodes<Type> mergeVisit(Nodes2<Type> that)
	{
		return
			new Nodes1<>
				(
					new NodeInternal3<>
						(
							that.node0,
							that.node1,
							this.node0
						)
				);
	}

	@Override
	public Nodes<Type> merge(Nodes<Type> that)
	{
		return that.mergeVisit(this);
	}
}