package nl.novit.util.collections.set;

import java.util.Optional;
import java.util.stream.Stream;

public interface Set<Type>
	extends Iterable<Type>, Comparable<Set<Type>>
{
	Set<Type> add(Type value);

	Set<Type> add(Iterable<? extends Type> values);

	boolean contains(Type value);

	boolean contains(Iterable<? extends Type> values);

	boolean equals(Set<Type> that);

	Optional<Type> first();

	Set<Type> headSet(Type value);

	boolean isEmpty();

	Optional<Type> last();

	Set<Type> remove(Type value);

	Set<Type> remove(Iterable<? extends Type> values);

	long size();

	Stream<Type> stream();

	Set<Type> tailSet(Type value);
}