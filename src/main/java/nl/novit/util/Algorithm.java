package nl.novit.util;

import nl.novit.util.collections.set.setavl.SetAVL;
import nl.novit.util.collections.set.setavl.SetAVLComparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class IteratorCombinations<Type>
	implements Iterator<List<Type>>
{
	public final List<Type> list;
	public final int size;
	public final Stack<Integer> indices;
	public SetAVL<Integer> indicesChosen;
	public SetAVL<Integer> indicesLeft;
	public List<Type> next;

	public IteratorCombinations(List<Type> list, int size)
	{
		this.list = list;
		this.size = size;
		this.indices = new Stack<>();
		this.indicesChosen = new SetAVLComparable<>();
		this.indicesLeft = new SetAVLComparable<>();
		for (int index = 0; index < this.list.size(); index++)
		{
			this.indicesLeft = this.indicesLeft.add(index);
		}
		for (int index = 0; index < size; index++)
		{
			this.indices.add(index);
			this.indicesChosen = this.indicesChosen.add(index);
			this.indicesLeft = this.indicesLeft.remove(index);
		}
		setNext();
		;
	}

	public void setNext()
	{
		this.next = this.indicesChosen
			.stream()
			.map(list::get)
			.collect(Collectors.toList());
	}

	@Override
	public boolean hasNext()
	{
		return this.next != null;
	}

	public boolean increaseIndices()
	{
		boolean result;
		if (this.indices.isEmpty())
		{
			result = false;
		}
		else
		{
			int index = this.indices.pop();
			this.indicesChosen = this.indicesChosen.remove(index);
			SetAVL<Integer> indicesLeftNext = this.indicesLeft.tailSet(index);
			this.indicesLeft = this.indicesLeft.add(index);
			if (0 < this.size - this.indicesChosen.size() - indicesLeftNext.size())
			{
				result = increaseIndices();
				if (result)
				{
					index = this.indicesLeft.tailSet(this.indicesChosen.last().orElse(null)).first().orElse(null);
				}
			}
			else
			{
				result = true;
				index = indicesLeftNext.first().orElse(null);
			}
			this.indicesLeft = this.indicesLeft.remove(index);
			this.indicesChosen = this.indicesChosen.add(index);
			this.indices.push(index);
		}
		return result;
	}

	@Override
	public List<Type> next()
	{
		List<Type> result = this.next;
		if (increaseIndices())
		{
			setNext();
		}
		else
		{
			this.next = null;
		}
		return result;
	}
}

class IteratorPermutations<Type>
	implements Iterator<List<Type>>
{
	public final List<Type> next;
	public final int[] c;
	public int index;

	public IteratorPermutations(List<Type> list)
	{
		this.next = new ArrayList<>(list);
		this.c = new int[list.size()];
		this.index = 1;
	}

	@Override
	public boolean hasNext()
	{
		return this.index < c.length;
	}

	@Override
	public List<Type> next()
	{
		List<Type> result = new ArrayList<>(this.next);
		while (hasNext() && this.index <= this.c[this.index])
		{
			this.c[index] = 0;
			this.index += 1;
		}
		if (hasNext())
		{
			if (this.index % 2 == 0)
			{
				Collections.swap(this.next, 0, this.index);
			}
			else
			{
				Collections.swap(this.next, this.c[this.index], this.index);
			}
			this.c[this.index] += 1;
			this.index = 1;
		}
		return result;
	}
};

public class Algorithm
{
	public static int binarySearchMaximum(Predicate1<Integer> filter, int start, int end)
	{
		return -binarySearchMinimum(x -> filter.apply(-x), -end + 1, -start + 1);
	}

	public static int binarySearchMinimum(Predicate1<Integer> filter, int start, int end)
	{
		int result;
		if (start == end)
		{
			result = end;
		}
		else
		{
			int middle = start + (end - start) / 2;
			if (filter.apply(middle))
			{
				result = binarySearchMinimum(filter, start, middle);
			}
			else
			{
				result = binarySearchMinimum(filter, middle + 1, end);
			}
		}
		return result;
	}

	public static <Type> Stream<List<Type>> combinations(List<Type> list, int size)
	{
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new IteratorCombinations<>(list, size), Spliterator.NONNULL), false);
	}

	public static long faculty(int n)
	{
		return n == 0 ? 1 : n * faculty(n - 1);
	}

	public static long gcd(long a, long b)
	{
		return b == 0 ? a : gcd(b, a % b);
	}

	public static <T> List<T> permutation(long permutation, List<T> list)
	{
		List<T> copy = new ArrayList<>(list);
		List<T> result = new ArrayList<>();
		for (int indexTo = 0; indexTo < list.size(); indexTo++)
		{
			int indexFrom = (int) permutation % copy.size();
			permutation = permutation / copy.size();
			result.add(copy.remove(indexFrom));
		}
		return result;
	}

	public static <Type> Stream<List<Type>> permutations(final List<Type> list)
	{
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new IteratorPermutations<>(list), Spliterator.NONNULL), false);
	}
}