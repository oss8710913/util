package nl.novit.util;

import java.math.BigInteger;

public class Fraction
	implements Comparable<Fraction>
{
	public static BigInteger gcd(BigInteger x, BigInteger y)
	{
		return y.equals(BigInteger.ZERO) ? x : gcd(y, x.mod(y));
	}

	public final BigInteger nominator;
	public final BigInteger denominator;

	public Fraction(BigInteger nominator, BigInteger denominator)
	{
		if (denominator.compareTo(BigInteger.ZERO) < 0)
		{
			nominator = nominator.negate();
			denominator = denominator.negate();
		}
		BigInteger gcd = gcd(nominator, denominator);
		this.nominator = nominator.divide(gcd);
		this.denominator = denominator.divide(gcd);
	}

	public Fraction(BigInteger nominator)
	{
		this(nominator, BigInteger.ONE);
	}

	@Override
	public int compareTo(Fraction that)
	{
		return this.nominator.multiply(that.denominator).compareTo(that.nominator.multiply(this.denominator));
	}

	public boolean equals(Fraction that)
	{
		return compareTo(that) == 0;
	}

	@Override
	public boolean equals(Object that)
	{
		return that instanceof Fraction && equals((Fraction) that);
	}

	public Fraction add(Fraction that)
	{
		return new Fraction(this.nominator.multiply(that.denominator).add(that.nominator.multiply(this.denominator)), this.denominator.multiply(that.denominator));
	}

	public Fraction negate()
	{
		return new Fraction(this.nominator.negate(), this.denominator);
	}

	public Fraction subtract(Fraction that)
	{
		return add(that.negate());
	}

	public Fraction multiply(Fraction that)
	{
		return new Fraction(this.nominator.multiply(that.nominator), this.denominator.multiply(that.denominator));
	}

	public Fraction divide(Fraction that)
	{
		return new Fraction(this.nominator.multiply(that.denominator), this.denominator.multiply(that.nominator));
	}

	@Override
	public String toString()
	{
		return this.nominator + "/" + this.denominator;
	}
}