package nl.novit.util;

public class Tuple5<Type0, Type1, Type2, Type3, Type4>
{
	public final Type0 value0;
	public final Type1 value1;
	public final Type2 value2;
	public final Type3 value3;
	public final Type4 value4;

	public Tuple5(Type0 value0, Type1 value1, Type2 value2, Type3 value3, Type4 value4)
	{
		this.value0 = value0;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
	}

	@Override
	public boolean equals(Object that)
	{
		boolean result = that instanceof Tuple5;
		if (result)
		{
			Tuple5 tuple = (Tuple5) that;
			result &= this.value0.equals(tuple.value0);
			result &= this.value1.equals(tuple.value1);
			result &= this.value2.equals(tuple.value2);
			result &= this.value3.equals(tuple.value3);
			result &= this.value4.equals(tuple.value4);
		}
		return result;
	}

	public String toString()
	{
		return "(" + this.value0.toString() + ", " + this.value1.toString() + ", " + this.value2.toString() + ", " + this.value3.toString() + ", " + this.value4.toString() + ")";
	}
}