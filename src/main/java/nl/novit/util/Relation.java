package nl.novit.util;

import nl.novit.util.collections.set.Map;
import nl.novit.util.collections.set.Set;
import nl.novit.util.collections.set.setavl.MapAVLComparable;
import nl.novit.util.collections.set.setavl.SetAVLComparable;

import java.util.Optional;

public class Relation<TypeA extends Comparable<? super TypeA>, TypeB extends Comparable<? super TypeB>> // TODO make persistent
{
	public Map<TypeA, Set<TypeB>> mapA2B;
	public Map<TypeB, Set<TypeA>> mapB2A;

	public Relation()
	{
		this.mapA2B = new MapAVLComparable<>();
		this.mapB2A = new MapAVLComparable<>();
	}

	public boolean add(TypeA a, TypeB b)
	{
		Set<TypeB> setB;
		Optional<Set<TypeB>> optionalSetB = this.mapA2B.getKey(a);
		if (optionalSetB.isPresent())
		{
			setB = optionalSetB.get();
		}
		else
		{
			setB = new SetAVLComparable<>();
			this.mapA2B = this.mapA2B.addKey(a, setB);
		}
		Set<TypeA> setA;
		Optional<Set<TypeA>> optionalSetA = this.mapB2A.getKey(b);
		if (optionalSetA.isPresent())
		{
			setA = optionalSetA.get();
		}
		else
		{
			setA = new SetAVLComparable<>();
			this.mapB2A = this.mapB2A.addKey(b, setA);
		}
		boolean result = setA.contains(a);
		if (!result)
		{
			this.mapB2A = this.mapB2A.addKey(b, setA.add(a));
			this.mapA2B = this.mapA2B.addKey(a, setB.add(b));
		}
		return result;
	}

	public Iterable<TypeA> allFirst()
	{
		return this.mapA2B.keys();
	}

	public Iterable<TypeB> allSecond()
	{
		return this.mapB2A.keys();
	}

	public Set<TypeB> getByFirst(TypeA a)
	{
		return this.mapA2B.getKey(a).orElse(new SetAVLComparable<>());
	}

	public Set<TypeA> getBySecond(TypeB b)
	{
		return this.mapB2A.getKey(b).orElse(new SetAVLComparable<>());
	}
}