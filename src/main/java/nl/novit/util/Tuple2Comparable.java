package nl.novit.util;

public class Tuple2Comparable<Type0 extends Comparable<? super Type0>, Type1 extends Comparable<? super Type1>>
	extends Tuple2<Type0, Type1>
	implements Comparable<Tuple2Comparable<Type0, Type1>>
{
	public Tuple2Comparable(Type0 left, Type1 right)
	{
		super(left, right);
	}

	@Override
	public int compareTo(Tuple2Comparable<Type0, Type1> that)
	{
		int result = Util.compareNullable(this.value0, that.value0);
		if (result == 0)
		{
			result = Util.compareNullable(this.value1, that.value1);
		}
		return result;
	}
}