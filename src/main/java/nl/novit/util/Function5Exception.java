package nl.novit.util;

public interface Function5Exception<TypeTarget, Type0, Type1, Type2, Type3, Type4>
{
	TypeTarget apply(Type0 value0, Type1 value1, Type2 value2, Type3 value3, Type4 value4)
		throws Exception;
}