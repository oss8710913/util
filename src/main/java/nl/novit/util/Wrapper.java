package nl.novit.util;

public class Wrapper<Type>
{
	public Type value;

	public Wrapper(Type value)
	{
		this.value = value;
	}

	public Wrapper()
	{
		this(null);
	}
}