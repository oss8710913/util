package nl.novit.util;

public interface Function4<TypeTarget, Type0, Type1, Type2, Type3>
{
	TypeTarget apply(Type0 value0, Type1 value1, Type2 value2, Type3 value3);
}